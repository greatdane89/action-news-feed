<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       
 * @since      1.0.0
 *
 * @package    Action_news_feed
 * @subpackage Action_news_feed/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Action_news_feed
 * @subpackage Action_news_feed/public
 * @author     David Baty
 */
class Action_news_feed_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $action_news_feed    The ID of this plugin.
	 */
	private $action_news_feed;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $action_news_feed       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $action_news_feed, $version ) {

		$this->action_news_feed = $action_news_feed;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in action_news_feed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The action_news_feed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->action_news_feed, plugin_dir_url( __FILE__ ) . 'css/action-news-feed-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in action_news_feed_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The action_news_feed_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->action_news_feed, plugin_dir_url( __FILE__ ) . 'js/action-news-feed-public.js', array( 'jquery' ), $this->version, false );

	}

}
