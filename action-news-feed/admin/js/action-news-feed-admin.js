
    function anfQuickImport() {

        jQuery("#anf_quick_import_tbl .anfStatusCheck").html('');
        jQuery("#anf_quick_import_tbl  .anfspinner").show();
        jQuery("#resultTable").hide();

        //variables
        let anfSearchQuery = jQuery("#anfQuickImportQuery").val();
        let anftype = jQuery("#anfImportType").val();
        let anfCategory = jQuery("#anfCategoryType").val();
        let anfNumber2import = jQuery("#anfNumber2import").val();
        let anfImportStatus =  jQuery("#anfImportStatus").val();

                jQuery.ajax({
                    type: 'POST',
                    async: true,
                    url: ajax_object.ajax_url,
                    cache: false,
                    data: {
                        action: 'anf_quick_import_action',
                        security: ajax_object.security,
                        whatever: 'test string',
                        anfSearchQuery: anfSearchQuery,
                        anfType: anftype,
                        anfcategory:  anfCategory,
                        anfNumber2import: anfNumber2import,
                        anfImportStatus: anfImportStatus


                        //whatever: ajax_object.we_value

                    },
                    success:function(response)   {

                        //console.log('Got this from the servers: ' + response);
                        jQuery("#resultTable").show();

                        //jQuery("#daveresult").append(response);
                       const results = document.getElementById('daveresult');//reults box

                        const people = JSON.parse(response);

                        const t = people.map((str) => {
                            results.insertAdjacentHTML('afterend', '<tr><td height="100" width="250">' +  str.post_title + '</td><td height="100" width="250"><img style="max-width: 100%;" src="' + str.post_image + '"/></td><td height="100" style="overflow: scroll;">' +  str.post_text + '</td></tr>');
                        });
                        

                        jQuery("#anf_quick_import_tbl .anfspinner").hide();

                        jQuery("#anf_quick_import_tbl  .anfStatusCheck").html('<span class="dashicons dashicons-yes"></span>');

                    },
                    error: function( response  ) {

                        console.log(response); 

                       
                        jQuery("#anf_quick_import_tbl .anfStatusCheck").html('<span class="dashicons dashicons-no"></span>');
                        jQuery("#anf_quick_import_tbl .anfspinner").hide();
                    }
            });

    }



    //allow function inside concat without autmaticly runniing the function
    function preventJS(html) {
        return html.replace(/<script(?=(\s|>))/i, '<script type="text/xml" ');
    }


    //build the category select options - feeds of of localized json -> ajax_object.we_value)
    function buildCatSelect(id) {

        let catsArray =  JSON.parse(ajax_object.we_value);
        const x = document.getElementById("anf-cat-sel-"+ id +"");

        const t = catsArray.map((str) => {
            let option = document.createElement("option");
            option.value = str.name;
            option.text = str.name;
            x.add(option);
        });

        x.value = "Uncategorised";

    }


    //add row
    function anfRunScheduleNow(event) {

        var t = 0;
        event.preventDefault();

                jQuery.ajax({
                    type: 'POST',
                    async: true,
                    url: ajax_object.ajax_url,
                    cache: false,
                    data: {
                        action: 'anf_run_schedule_now',
                        anf_rows: 'test string',
                        security: ajax_object.security,
                        //whatever: ajax_object.we_value

                    },
                    success:function(response)   {


                       // buildCatSelect(people.id);
                           location.reload();

  
                    },
                    error: function() {
                        alert("fail - contact the developer for assistance");
                    }
            });
    }


    //delete shecdled row
    function anf_delete_schudule(event, id, d) {
        event.preventDefault();

        var id = id;

                jQuery.ajax({
                    type: 'POST',
                    async: true,
                    url: ajax_object.ajax_url,
                    cache: false,
                    data: {
                        action: 'anf_delete_schudule',
                        delete_id: id,
                        security: ajax_object.security,
                        //whatever: ajax_object.we_value
                    },
                    success:function(response)   {

                        d.closest('tr').remove();
                        //jQuery("#res").html(response);
                       // alert('Got this from the servers: ' + response);
                        
                    },
                    error: function() {
                        alert(" fail");
                    }
            });

    }


    //gets submitted form data -> wp-options
    function getFormData($form){
        var unindexed_array = $form.serializeArray();
        var indexed_array = {};

        jQuery.map(unindexed_array, function(n, i){
            indexed_array[n['name']] = n['value'];
        });

        return indexed_array;
    }

    //dsave schedule functions
    function anf_save_schedule(event) {
        event.preventDefault();

        var t = jQuery('#scheduleOptionsForm').serialize();
        var form = jQuery("#scheduleOptionsForm");
        var data = getFormData(form);

                jQuery.ajax({
                    type: 'POST',
                    async: true,
                    url: ajax_object.ajax_url,
                    cache: false,
                    data: {
                        action: 'anf_save_schedule',
                        whatever: data,
                        security: ajax_object.security,
                    },
                    success:function(response)   {

                        alert('settings saved' );
                        
                    },
                    error: function() {
                        alert(" fail");
                    }
            });

}

//run a scheduled event on demand
function anf_run_schudule_now(event, id,) {
    event.preventDefault();



        jQuery("#" + id + "").find(".anfStatusCheck").html('');
        jQuery("#" + id + "").find(".anfspinner").show();
        jQuery("#" + id + "").find("#resultTable").hide();

    const query = jQuery("#" + id + "").find(".anf_schedule_input_query").val();
    const type = jQuery("#" + id + "").find(".anf_schedule_input_type").val();
    const category = jQuery("#" + id + "").find(".anf_schedule_input_category").val();
    const import_limit = jQuery("#" + id + "").find(".anf_schedule_input_import_limit").val();
    const post_status = jQuery("#" + id + "").find(".anf_schedule_input_post_status").val();


                jQuery.ajax({
                    type: 'POST',
                    async: true,
                    url: ajax_object.ajax_url,
                    cache: false,
                    data: {
                        action: 'anf_run_schedule_ondemand',
                        query: query,
                        type: type,
                        category: category,
                        import_limit: import_limit,
                        post_status: post_status,
                        security: ajax_object.security,
                    },
                    success:function(response)   {

                        //alert('got this from the server' + response + '');



           jQuery("#resultTable").show();

                        //jQuery("#daveresult").append(response);
                       const results = document.getElementById('daveresult');//reults box

                        const people = JSON.parse(response);

                        const t = people.map((str) => {
                            results.insertAdjacentHTML('afterend', '<tr><td height="100" width="250">' +  str.post_title + '</td><td height="100" width="250"><img style="max-width: 100%;" src="' + str.post_image + '"/></td><td height="100" style="overflow: scroll;">' +  str.post_text + '</td></tr>');
                        });                        
                        


                        jQuery("#" + id + "").find(".anfspinner").hide();

                        jQuery("#" + id + "").find(".anfStatusCheck").html('<span class="dashicons dashicons-yes"></span>');



                    },
                    error: function() {
                        //alert(" fail");
                        jQuery("#" + id + "").find(".anfStatusCheck").html('<span class="dashicons dashicons-no"></span>');
                    }
            });    
}




//on delete all posts created by this plugin
function anf_delete_created_posts_js() {

let anf_confirm = confirm("Are you sure, this cannot be undone! We suggest you make a backup first");

if (anf_confirm == true) {
        jQuery("#anf_deleteAll .anfStatusCheck").html('');
        jQuery("#anf_deleteAll .anfspinner").show();

                    jQuery.ajax({
                    type: 'POST',
                    async: true,
                    url: ajax_object.ajax_url,
                    cache: false,
                    data: {
                        action: 'anf_delete_created_posts',
                        query: 'emptyString',
                        security: ajax_object.security,
                    },
                    success:function(response)   {

                        //alert('got this from the server' + response + '');
                        //alert(response);


                        jQuery("#anf_deleteAll .anfspinner").hide();

                        jQuery("#anf_deleteAll .anfStatusCheck").html('<span class="dashicons dashicons-yes"></span> <span>Deleted</span>');
                        
                    },
                    error: function() {
                        alert(" fail");
                    }
            });   
}                    
}




function anf_reset_schedule_js( event ) {
        event.preventDefault();
                        jQuery.ajax({
                    type: 'POST',
                    async: true,
                    url: ajax_object.ajax_url,
                    cache: false,
                    data: {
                        action: 'anf_reset_schedule',
                        resetIt: 'emptyString',
                        security: ajax_object.security,
                    },
                    success:function(response)   {

                        alert(response);
                                     location.reload();
                        
                    },
                    error: function() {
                        alert(" fail");
                    }
            }); 
}


/*

function anf_trigger_modal( id ) {

    jQuery("#api-add-param-" + id + "").show();



    const queryInput = document.getElementById("anf_schedule_input_query_" + id + "");
    const InputVal = document.getElementById("api-add-param-" + id + "");

//on select
if (InputVal.addEventListener) {
  InputVal.addEventListener(
    "change",
    () => {
      RealChange(event);
    },
    false
  );
}

const RealChange = event => {
  //alert(event.target.value);
  let y =  queryInput.value;
  //alert(y);
  let e = event.target.value;
  //queryInput.value = e;
  queryInput.value = y + ' ' + e;

  InputVal.style.display="none";
};




}


*/


/**
 * Returns PBKDF2 derived key from supplied password.
 *
 * Stored key can subsequently be used to verify that a password matches the original password used
 * to derive the key, using pbkdf2Verify().
 *
 * @param   {String} password - Password to be hashed using key derivation function.
 * @param   {Number} [iterations=1e6] - Number of iterations of HMAC function to apply.
 * @returns {String} Derived key as base64 string.
 *
 * @example
 *   
 */
function anf_trigger_modal_quick() {

    jQuery(".quikQueryopts").show();

    //Vars
    const anf_inputVal = document.getElementById('anf_search');//input box
    const results = document.getElementById('results');//reults box
    const filter = document.getElementById('filter');//sort button
    const people = [
        { "name": "language", "des": "The language of the post. The default is any", "element": "select"},
        { "name": "author",  "des": "Return posts written by a specific author", "element": "text"},
        { "name": "has video", "des": "A Boolean parameter that returns results for web pages containing embedded video.", "element": "truefalse"},
        { "name": "external links",  "des": "Returns posts that link to specified URL.", "element": "truefalse"},
        { "name": "is first",  "des": "A Boolean parameter that specifies if to search only on the first post (exclude the comments)", "element": ""},
        { "name": "rating", "des": "Filter online reviews (site_type:discussions) by the star rating, a floating number between 0.0 and 5.0 (normalized if review scale exceeds 5)", "element": ""},
        { "name": "published",  "des": "Filter posts by date of publication.", "element": ""},
        { "name": "site type",  "des": "Filter by data feed; News, blogs, discussions.", "element": ""},
        { "name": "site",  "des": "Filter by domain source.", "element": ""},
        { "name": "country",  "des": "ountry associated with a given source based on TLD and language. Default country for dot-com URLs in English is USA.", "element": ""},
        { "name": "site suffix",  "des": "Filter by top level domain (TLD).", "element": ""},
        { "name": "site category",  "des": "Limit the results to posts originating from sites categorized as one (or more)", "element": ""},
        { "name": "domain rank",  "des": "Filter by traffic rank (lower rank indicates higher traffic).", "element": ""},
        { "name": "Thread title",  "des": "A textual Boolean query describing the keywords that should (or shouldn’t) appear in the thread title.", "element": ""},
        { "name": "thread Section Title",  "des": "A textual Boolean query describing the keywords that should (or shouldn’t) appear in the site’s section where the post was published", "element": ""},
        { "name": "thread URL",  "des": "Get all the posts of a specific thread (note that you must escape the http:// part of the URL like so: http\\:\\\/\\\/)", "element": ""},
        { "name": "spam score",  "des": "A score value between 0 to 1, indicating how spammy the thread text is.", "element": ""},
        { "name": "thread published",  "des": "Filter threads (root post and replies) by date of publication.", "element": ""},
        { "name": "performance score",  "des": "A virality score for news and blogs posts only. The score ranges between 0-10, where 0 means that the post didn't do well at all, i.e rarely or was never shared, to 10 which means that the post was on fire, shared thousands of times on Facebook.", "element": ""},
        { "name": "entity: person",  "des": "Filter by person name (recommended only for disambiguation, otherwise you should use a simple keyword search)", "element": ""},
        { "name": "entity: organization",  "des": "Filter by Organization", "element": ""},
        { "name": "entity: location",  "des": "Filter by location name (recommended only for disambiguation, otherwise you should use a simple keyword search)", "element": ""},
        { "name": "social filters",  "des": "Filter by social likes, shares, or Facebook comments", "element": ""}
    ];

    /*========================================================
    ------------------on input type/search-------------------
    =========================================================*/

        if (anf_inputVal.addEventListener) {
            anf_inputVal.addEventListener('input', () => {
                anf_searh_realtime();
            }, false);
        } else if (anf_inputVal.attachEvent) {  
            anf_inputVal.attachEvent('onpropertychange', () => {
                anf_searh_realtime();
            });
        }
    /*---------------------------------------------------------*/


    /*=========================================================
    -------------------filter input----------------------------
    ===========================================================*/

    const anf_searh_realtime = () => {
        const v = anf_inputVal.value;//input value
            results.innerHTML="";
    
            /*takes people object and filters for name that includes user search input -> maps results and appends li to results*/
        const t = people.filter((x) => x.name.includes(v)).map((str) => {
            results.insertAdjacentHTML('afterbegin', '<div class="qb-option qb-add-option" data-add-option="language"><div class="qb-option-title">' +  str.name + '</div><div class="qb-key-option-desc">' + str.des + '</div></div>');
        });
  
        if(t.length < 1) {
          results.insertAdjacentHTML('afterbegin', 'No Results');
        }

    }
    anf_searh_realtime();
    /*------------------------------------------------------------------*/
}


/**
 * 
 *
 *   
 */
async function anf_closequikQueryopts() {
    await jQuery(".quikQueryopts").hide();
}
