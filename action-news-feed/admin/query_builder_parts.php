<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       
 * @since      1.0.0
 *
 * @package    Action_news_feed
 * @subpackage Action_news_feed/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Action_news_feed
 * @subpackage Action_news_feed/admin
 * @author     David Baty
 */
class Action_news_feed_query_builder_parts {


   public function anf_build_query_ex_table() {
      ?>


<table id="apiTableSocial" class="wp-list-table widefat striped">
<thead>
   <tr>
      <th>Description</th>
      <th>Example Query</th>
   </tr>
</thead>
<tbody>

<tr>
   <td>Find posts mentioning the phrase "United States" in either CNN, Yahoo or Reuters</td>
   <td>(site:yahoo.com OR site:cnn.com OR site:reuters.com) "United States"
</td>
</tr>
<tr>
   <td>Find news articles in English from sites categorized as sports sites.</td>
   <td>site_category:sports language:english site_type:news</td>
</tr>
<tr>
   <td>Find posts (not comments) that contain a video, from the top 1,000 sites by traffic</td>
   <td>has_video:true domain_rank: &gt; 1000 is_first:true</td>
</tr>
<tr>
   <td>Find reviews with rating lower than 2, mentioning either Apple, iPhone or iPad</td>
   <td> rating: &gt; 2 (Apple OR iphone OR ipad) </td>
</tr>
<tr>
   <td>Find reviews with score equal or higher than for on Google Play store</td>
   <td>rating:>=4 site_full:play.google.com</td>
</tr>
<tr>
   <td>Find posts in English that mention the phrase "Big Data" in the title but not in the post itself</td>
   <td>title:"big data" -text:"big data" language:english</td>
</tr>
<tr>
   <td>Find forum topics (first posts) in English with a minimum 10 participants and more than 20 replies</td>
   <td>participants_count:>10 replies_count:>20 site_type:discussions is_first:true language:english</td>
</tr>
<tr>
   <td>Find news articles that contain the phrase "Data Security" or "Cloud", and not "Apple"</td>
   <td>("Data Security" OR Cloud) -Apple site_type:news</td>
</tr>
<tr>
   <td>Find news or blog posts in English in domains ending with the .fr TLD (France)</td>
   <td>language:english site_suffix:fr (site_type:news OR site_type:blogs)</td>
</tr>
<tr>
   <td>Find forum posts originating from message boards in France</td>
   <td>thread.country:FR (site_type:discussions)</td>
</tr>
<tr>
   <td>Find news posts, with a minimum number of 10 LinkedIn shares or more than 100 Facebook shares.</td>
   <td>site_type:news (social.linkedin.shares:>10 OR social.facebook.likes:>100)</td>
</tr>
<tr>
   <td>Find news or blog posts, with a minimum number of 100 Facebook likes, that mention the keyword "peace"</td>
   <td>peace social.facebook.likes:>100</td>
</tr>
<tr>
   <td>Find viral news articles that contain the phrase "United States" or "US" in the title</td>
   <td>performance_score:>0 (title:"United States" OR title:US)</td>
</tr>
</tbody>
</table>
<br>


<h2><label for="apiTableSocial">Social Filters</label></h2>


<table id="apiTableSocial" class="wp-list-table widefat striped">
   <thead>
<tr>
   <th>Parameter</th>
   <th>Description</th>
   <th>Example</th>
   </div>
</tr>
</thead>
<tbody>
<tr>
   <td><p>performance_score</p> </td>
   <td><p>A virality score for news and blogs posts only. The score ranges between 0-10, where 0 means that the post didn't do well at all, i.e rarely or was never shared, to 10 which means that the post was on fire, shared thousands of times on Facebook.</p></td>
   <td><p>Search for news or blog posts with performance score higher than 8 (highly viral):</p><p>apple performance_score:&gt;8</p></td>
<tr>
   <td><p>social.facebook.likes</p></td>
   <td><p>Return posts filtered by the number of Facebook likes.</p></td>
   <td><p>Return posts with more than 10 Facebook likes:<br>social.facebook.likes:&gt;10</p></td>
</tr>   
<tr>
   <td><p>social.facebook.shares</p></td>
   <td><p>Return posts filtered by the number of Facebook shares.</p></td>
   <td><p>Return posts with more than 10 Facebook shares:<br>social.facebook.shares:&gt;10</p></td>
</tr>  
<tr>
   <td><p>social.facebook.comments</p></td>
   <td><p>Return posts filtered by the number of Facebook comments.</p></td>
   <td><p>Return posts with more than 10 Facebook comments:<br>social.facebook.comments:&gt;10</p></td>
</tr>
<tr>
   <td><p>social.pinterest.shares</p></td>
   <td><p>Return posts filtered by the number of Pinterest shares.</p></td>
   <td><p>Return posts with more than 10 Pinterest shares:<br>social.pinterest.shares:&gt;10</p></td>
 </tr>
<tr>
   <td><p>social.stumbledupon.shares</p></td>
   <td><p>Return posts filtered by the number of Stumbledupon shares.</p></td>
   <td><p>Return posts with more than 10 Stumbledupon shares:<br>social.stumbledupon.shares:&gt;10</p></td>
</tr>
<tr>
   <td><p>social.vk.shares</p></td>
   <td><p>Return posts filtered by the number of VK shares.</p></td>
   <td><p>Return posts with more than 10 VK shares:<br>social.vk.shares:&gt;10</p></td>
</tr>   
</body>
</table>

<br>
<h2><label for="">Entities & Sentiment Filters</label></h2>
<table id="apiTableSocial" class="wp-list-table widefat striped">
   <thead>
<tr>
   <th>Parameter</th>
   <th>Description</th>
   <th>Example</th>
   </div>
</tr>
</thead>
<tbody>
   <tr>
      <td>person</td>
      <td>Filter by person name. You should use this filter only for disambiguation, otherwise you should use a simple keyword search.</td>
      <td>person:"barack obama"</td>
   </tr>
   <tr>
      <td>organization</td>
      <td>Filter by organization/company name. You should use this filter only for disambiguation, otherwise you should use a simple keyword search.</td>
      <td>organization:"apple"</td>
   </tr>
   <tr>
      <td>location</td>
      <td>Filter by location name Important: Don't confuse this with the country filter. If you want to search for sites from a specific country, use the thread.country parameter (explained above).</td>
      <td>location:"germany"</td>
   </tr>
   <tr>
      <td>[entity].[sentiment]</td>
      <td>Find an entity with a sentiment context attached to it.</td>
      <td>person.positive:"obama"
organization.negative:"apple"
organization.neutral:"google"</td>
   </tr>    
</tbody>
</table>


<br>
<h2><label for="">Thread Filters</label></h2>
<table id="apiTableSocial" class="wp-list-table widefat striped">
   <thead>
<tr>
   <th>Parameter</th>
   <th>Description</th>
   <th>Example</th>
   </div>
</tr>
</thead>
<tbody>
   <tr>
      <td>thread.title</td>
      <td>  
A textual Boolean query describing the keywords that should (or shouldn’t) appear in the thread title.</td>
      <td>Search for posts containing the word "glass" and not "metal" in their title:

thread.title:glass -thread.title:metal</td>
   </tr> 
   <tr>
         <td>thread.section_title

</td>
         <td>A textual Boolean query describing the keywords that should (or shouldn’t) appear in the site’s section where the post was published

</td>
         <td>Search for the posts containing the word food only under sections with a title that contains the word "restaurants":

food AND thread.section_title:restaurants</td>
      </tr> 
      <tr>
         <td>thread.url</td>
         <td>Get all the posts of a specific thread (note that you must escape the http:// part of the URL like so: http\:\/\/).</td>
         <td></td>
      </tr>
      <tr>
         <td>spam_score</td>
         <td>A score value between 0 to 1, indicating how spammy the thread text is.

</td>
         <td>Return threads with spam score lower or equals to 0.8:

spam_score:<=0.8</td>
      </tr>
      <tr>
         <td>thread.published</td>
         <td>A time-stamp (in milliseconds) enable you to filter threads that were published before or after certain date/time.</td>
         <td>Return threads published after Thu, 30 Mar 2017 09:16:28 GMT: thread.published:>
1490865388000</td>
      </tr>
      <tr>
         <td>crawled</td>
         <td>A time-stamp (in milliseconds) enable you to filter posts that were crawled before or after certain date/time.</td>
         <td>Return posts crawled after Thu, 30 Mar 2017 09:16:28 GMT:
crawled:>1490865388000</td>
      </tr>
</tbody>
</table>
<br>
<h2><label for="">Site Filters</label></h2>
<table id="apiTableSocial" class="wp-list-table widefat striped">
   <thead>
<tr>
   <th>Parameter</th>
   <th>Description</th>
   <th>Example</th>
   </div>
</tr>
</thead>
<tbody>
   <tr>
      <td>site_type</td>
      <td>What type of sites to search in (the default is any). Available Types: news - blogs - discussions</td>
      <td>Only news: site_type:news News & Blogs: (site_type:news OR site_type:blogs)</td>
   </tr>
   <tr>
      <td>site</td>
      <td>Limit the results to a specific site or sites.</td>
      <td>Limit the results to posts from Yahoo or CNN:
(site:yahoo.com OR site:cnn.com)</td>
   </tr>
      <tr>
      <td>thread.country

</td>
      <td>Webhose.io uses heuristics to determine the country origin of a site, by taking into account the site's IP, TLD and language. Many times the country origin isn't conclusive so it isn't set, therefor filtering by country may result in much less data than when filtering by language.

</td>
      <td>Return posts from sites from Hong Kong:
thread.country:HK

To get the full country code list, visit countrycode.org.</td>
   </tr>
      <tr>
      <td>site_suffix</td>
      <td>Limit the results to a specific site suffix</td>
      <td>Return posts from sites where their top level domain (TLD) ends with .fr:

site_suffix:fr</td>
   </tr>
      <tr>
      <td>site_full</td>
      <td>Filter sites based on the domain and optionally by their sub-domain

</td>
      <td>Return posts from Yahoo answers:

site_full:answers.yahoo.com</td>
   </tr>
      <tr>
      <td>site_category</td>
      <td>Limit the results to posts originating from sites categorized as one (or more)<a href="https://docs.webhose.io/v1.0/docs/site-category-values" target="_self">of the following</a></td>
      <td>Return posts from sites categorized as sports or games related:
(site_category:sports OR site_category:games)</td>
   </tr>
      <tr>
      <td>domain_rank</td>
      <td>A rank that specifies how popular a domain is (by monthly traffic)
</td>
      <td>Search for posts from the top 1,000 sites:domain_rank: &gt; 1000 </td>
   </tr> 
</tbody>
</table>


<br>
<h2><label for="">Site Filters</label></h2>
<table id="apiTableSocial" class="wp-list-table widefat striped">
   <thead>
<tr>
   <th>Parameter</th>
   <th>Description</th>
   <th>Example</th>
   </div>
</tr>
</thead>
<tbody>
   <tr>
<td>language</td>
<td>The language of the post. The default is any.</td>
<td>Find posts in French or Italian:(language:french OR language:italian)</td>
</tr>
   <tr>
<td>author</td>
<td>Return posts written by a specific author</td>
<td>Find posts written by Admin:
author:Admin</td>
</tr>
   <tr>
<td>text</td>
<td>  
A textual Boolean query describing the keywords that should (or shouldn’t) appear in the post text.</td>
<td>text:(apple OR android)

</td>
</tr>
   <tr>
<td>has_video</td>
<td>A Boolean parameter that specifies if to search only for posts that contain a video.

</td>
<td>has_video:true

</td>
</tr>
   <tr>
<td>external_links

</td>
<td>Search for posts that included links to another site.

</td>
<td>Search for posts that linked to LinkedIn (note that both the slashes and colons are prefixed by a backslash):

external_links:https\:\/\/www.linkedin.com*</td>
</tr>
   <tr>
<td>is_first</td>
<td>A Boolean parameter that specifies if to search only on the first post (exclude the comments)</td>
<td>is_first:true

</td>
</tr>
   <tr>
<td>rating</td>
<td>For review posts, the rating parameter provides the star rating for the review, a floating number between 0.0 to 5.0.</td>
<td>Return all the posts with rating greater than 0:

rating:>0</td>
</tr>
   <tr>
<td>published</td>
<td>A time-stamp (in milliseconds) enable you to filter posts that were published before or after certain date/time.

</td>
<td>Return posts published after Thu, 30 Mar 2017 09:16:28 GMT:
published:>1490865388000 </td>
</tr>
</tbody>
</table>

      <?
   

   }   

	public function anf_query_select_site_category() {
		?>

   <option value="site_category:3d_graphics">3-D Graphics</option>
   <option value="site_category:7_12_education">7-12 Education</option>
   <option value="site_category:a_d_d">A.D.D.</option>
   <option value="site_category:aids_hiv">AIDS/HIV</option>
   <option value="site_category:accessories">Accessories</option>
   <option value="site_category:adoption">Adoption</option>
   <option value="site_category:adult_education">Adult Education</option>
   <option value="site_category:adventure_travel">Adventure Travel</option>
   <option value="site_category:advertising">Advertising</option>
   <option value="site_category:africa">Africa</option>
   <option value="site_category:agriculture">Agriculture</option>
   <option value="site_category:air_travel">Air Travel</option>
   <option value="site_category:allergies">Allergies</option>
   <option value="site_category:alternative_medicine">Alternative Medicine</option>
   <option value="site_category:alternative_religions">Alternative Religions</option>
   <option value="site_category:american_cuisine">American Cuisine</option>
   <option value="site_category:animation">Animation</option>
   <option value="site_category:antivirus_software">Antivirus Software</option>
   <option value="site_category:apartments">Apartments</option>
   <option value="site_category:appliances">Appliances</option>
   <option value="site_category:aquariums">Aquariums</option>
   <option value="site_category:architects">Architects</option>
   <option value="site_category:art_history">Art History</option>
   <option value="site_category:art_technology">Art/Technology</option>
   <option value="site_category:arthritis">Arthritis</option>
   <option value="site_category:arts_and_crafts">Arts &amp; Crafts</option>
   <option value="site_category:entertainment">Arts &amp; Entertainment</option>
   <option value="site_category:asthma">Asthma</option>
   <option value="site_category:astrology">Astrology</option>
   <option value="site_category:atheism_agnosticism">Atheism/Agnosticism</option>
   <option value="site_category:australia_and_new_zealand">Australia &amp; New Zealand</option>
   <option value="site_category:autism_pdd">Autism/PDD</option>
   <option value="site_category:auto_parts">Auto Parts</option>
   <option value="site_category:auto_racing">Auto Racing</option>
   <option value="site_category:auto_repair">Auto Repair</option>
   <option value="site_category:vehicles">Automotive</option>
   <option value="site_category:babies_and_toddlers">Babies &amp; Toddlers</option>
   <option value="site_category:barbecues_and_grilling">Barbecues &amp; Grilling</option>
   <option value="site_category:baseball">Baseball</option>
   <option value="site_category:beadwork">Beadwork</option>
   <option value="site_category:beauty">Beauty</option>
   <option value="site_category:bed_and_breakfasts">Bed &amp; Breakfasts</option>
   <option value="site_category:beginning_investing">Beginning Investing</option>
   <option value="site_category:bicycling">Bicycling</option>
   <option value="site_category:biology">Biology</option>
   <option value="site_category:biotech_biomedical">Biotech/Biomedical</option>
   <option value="site_category:bipolar_disorder">Bipolar Disorder</option>
   <option value="site_category:birds">Birds</option>
   <option value="site_category:birdwatching">Birdwatching</option>
   <option value="site_category:games">Board Games/Puzzles</option>
   <option value="site_category:body_art">Body Art</option>
   <option value="site_category:bodybuilding">Bodybuilding</option>
   <option value="site_category:books_and_literature">Books &amp; Literature</option>
   <option value="site_category:botany">Botany</option>
   <option value="site_category:boxing">Boxing</option>
   <option value="site_category:brain_tumor">Brain Tumor</option>
   <option value="site_category:buddhism">Buddhism</option>
   <option value="site_category:budget_travel">Budget Travel</option>
   <option value="site_category:business">Business</option>
   <option value="site_category:business_software">Business Software</option>
   <option value="site_category:business_travel">Business Travel</option>
   <option value="site_category:buying_selling_cars">Buying/Selling Cars</option>
   <option value="site_category:buying_selling_homes">Buying/Selling Homes</option>
   <option value="site_category:by_us_locale">By US Locale</option>
   <option value="site_category:c_c_plus_plus">C/C++</option>
   <option value="site_category:cajun_creole">Cajun/Creole</option>
   <option value="site_category:cameras_and_camcorders">Cameras &amp; Camcorders</option>
   <option value="site_category:camping">Camping</option>
   <option value="site_category:canada">Canada</option>
   <option value="site_category:cancer">Cancer</option>
   <option value="site_category:candle_and_soap_making">Candle &amp; Soap Making</option>
   <option value="site_category:canoeing_kayaking">Canoeing/Kayaking</option>
   <option value="site_category:car_culture">Car Culture</option>
   <option value="site_category:gambling">Card Games</option>
   <option value="site_category:career_advice">Career Advice</option>
   <option value="site_category:career_planning">Career Planning</option>
   <option value="site_category:jobs">Careers</option>
   <option value="site_category:caribbean">Caribbean</option>
   <option value="site_category:catholicism">Catholicism</option>
   <option value="site_category:cats">Cats</option>
   <option value="site_category:celebrity_fan_gossip">Celebrity Fan/Gossip</option>
   <option value="site_category:cell_phones">Cell Phones</option>
   <option value="site_category:certified_pre_owned">Certified Pre-Owned</option>
   <option value="site_category:cheerleading">Cheerleading</option>
   <option value="site_category:chemistry">Chemistry</option>
   <option value="site_category:chess">Chess</option>
   <option value="site_category:chinese_cuisine">Chinese Cuisine</option>
   <option value="site_category:cholesterol">Cholesterol</option>
   <option value="site_category:christianity">Christianity</option>
   <option value="site_category:chronic_fatigue_syndrome">Chronic Fatigue Syndrome</option>
   <option value="site_category:chronic_pain">Chronic Pain</option>
   <option value="site_category:cigars">Cigars</option>
   <option value="site_category:climbing">Climbing</option>
   <option value="site_category:clothing">Clothing</option>
   <option value="site_category:cocktails_beer">Cocktails/Beer</option>
   <option value="site_category:coffee_tea">Coffee/Tea</option>
   <option value="site_category:cold_and_flu">Cold &amp; Flu</option>
   <option value="site_category:collecting">Collecting</option>
   <option value="site_category:college">College</option>
   <option value="site_category:college_administration">College Administration</option>
   <option value="site_category:college_life">College Life</option>
   <option value="site_category:comic_books">Comic Books</option>
   <option value="site_category:commentary">Commentary</option>
   <option value="site_category:comparison">Comparison</option>
   <option value="site_category:computer_certification">Computer Certification</option>
   <option value="site_category:computer_networking">Computer Networking</option>
   <option value="site_category:computer_peripherals">Computer Peripherals</option>
   <option value="site_category:computer_reviews">Computer Reviews</option>
   <option value="site_category:construction">Construction</option>
   <option value="site_category:contests_and_freebies">Contests &amp; Freebies</option>
   <option value="site_category:convertible">Convertible</option>
   <option value="site_category:copyright_infringement">Copyright Infringement</option>
   <option value="site_category:coupe">Coupe</option>
   <option value="site_category:couponing">Couponing</option>
   <option value="site_category:credit_debt_and_loans">Credit/Debt &amp; Loans</option>
   <option value="site_category:cricket">Cricket</option>
   <option value="site_category:crossover">Crossover</option>
   <option value="site_category:cruises">Cruises</option>
   <option value="site_category:cuisine_specific">Cuisine-Specific</option>
   <option value="site_category:data_centers">Data Centers</option>
   <option value="site_category:databases">Databases</option>
   <option value="site_category:dating">Dating</option>
   <option value="site_category:daycare_pre_school">Daycare/Pre School</option>
   <option value="site_category:deafness">Deafness</option>
   <option value="site_category:dental_care">Dental Care</option>
   <option value="site_category:depression">Depression</option>
   <option value="site_category:dermatology">Dermatology</option>
   <option value="site_category:desktop_publishing">Desktop Publishing</option>
   <option value="site_category:desktop_video">Desktop Video</option>
   <option value="site_category:desserts_and_baking">Desserts &amp; Baking</option>
   <option value="site_category:diabetes">Diabetes</option>
   <option value="site_category:diesel">Diesel</option>
   <option value="site_category:dining_out">Dining Out</option>
   <option value="site_category:distance_learning">Distance Learning</option>
   <option value="site_category:divorce_support">Divorce Support</option>
   <option value="site_category:dogs">Dogs</option>
   <option value="site_category:drawing_sketching">Drawing/Sketching</option>
   <option value="site_category:eastern_europe">Eastern Europe</option>
   <option value="site_category:education">Education</option>
   <option value="site_category:eldercare">Eldercare</option>
   <option value="site_category:electric_vehicle">Electric Vehicle</option>
   <option value="site_category:email">Email</option>
   <option value="site_category:engines">Engines</option>
   <option value="site_category:english_as_a_2nd_language">English as a 2nd Language</option>
   <option value="site_category:entertaining">Entertaining</option>
   <option value="site_category:entertainment">Entertainment</option>
   <option value="site_category:environmental_safety">Environmental Safety</option>
   <option value="site_category:epilepsy">Epilepsy</option>
   <option value="site_category:ethnic_specific">Ethnic Specific</option>
   <option value="site_category:europe">Europe</option>
   <option value="site_category:exercise">Exercise</option>
   <option value="site_category:adult">Extreme Graphic/Explicit Violence</option>
   <option value="site_category:family_and_parenting">Family &amp; Parenting</option>
   <option value="site_category:family_internet">Family Internet</option>
   <option value="site_category:fashion">Fashion</option>
   <option value="site_category:figure_skating">Figure Skating</option>
   <option value="site_category:financial_aid">Financial Aid</option>
   <option value="site_category:financial_news">Financial News</option>
   <option value="site_category:financial_planning">Financial Planning</option>
   <option value="site_category:fine_art">Fine Art</option>
   <option value="site_category:fly_fishing">Fly Fishing</option>
   <option value="site_category:food">Food &amp; Drink</option>
   <option value="site_category:food_allergies">Food Allergies</option>
   <option value="site_category:football">Football</option>
   <option value="site_category:forestry">Forestry</option>
   <option value="site_category:france">France</option>
   <option value="site_category:freelance_writing">Freelance Writing</option>
   <option value="site_category:french_cuisine">French Cuisine</option>
   <option value="site_category:freshwater_fishing">Freshwater Fishing</option>
   <option value="site_category:gerd_acid_reflux">GERD/Acid Reflux</option>
   <option value="site_category:game_and_fish">Game &amp; Fish</option>
   <option value="site_category:gardening">Gardening</option>
   <option value="site_category:gay_life">Gay Life</option>
   <option value="site_category:genealogy">Genealogy</option>
   <option value="site_category:geography">Geography</option>
   <option value="site_category:geology">Geology</option>
   <option value="site_category:getting_published">Getting Published</option>
   <option value="site_category:golf">Golf</option>
   <option value="site_category:government">Government</option>
   <option value="site_category:graduate_school">Graduate School</option>
   <option value="site_category:graphics_software">Graphics Software</option>
   <option value="site_category:greece">Greece</option>
   <option value="site_category:green_solutions">Green Solutions</option>
   <option value="site_category:guitar">Guitar</option>
   <option value="site_category:hatchback">Hatchback</option>
   <option value="site_category:hate_content">Hate Content</option>
   <option value="site_category:headaches_migraines">Headaches/Migraines</option>
   <option value="site_category:health">Health &amp; Fitness</option>
   <option value="site_category:health_lowfat_cooking">Health/Lowfat Cooking</option>
   <option value="site_category:heart_disease">Heart Disease</option>
   <option value="site_category:hedge_fund">Hedge Fund</option>
   <option value="site_category:herbs_for_health">Herbs for Health</option>
   <option value="site_category:hinduism">Hinduism</option>
   <option value="site_category:hobbies_and_interests">Hobbies &amp; Interests</option>
   <option value="site_category:holistic_healing">Holistic Healing</option>
   <option value="site_category:home_and_garden">Home &amp; Garden</option>
   <option value="site_category:home_recording">Home Recording</option>
   <option value="site_category:home_repair">Home Repair</option>
   <option value="site_category:home_theater">Home Theater</option>
   <option value="site_category:home_video_dvd">Home Video/DVD</option>
   <option value="site_category:homeschooling">Homeschooling</option>
   <option value="site_category:homework_study_tips">Homework/Study Tips</option>
   <option value="site_category:honeymoons_getaways">Honeymoons/Getaways</option>
   <option value="site_category:horse_racing">Horse Racing</option>
   <option value="site_category:horses">Horses</option>
   <option value="site_category:hotels">Hotels</option>
   <option value="site_category:human_resources">Human Resources</option>
   <option value="site_category:humor">Humor</option>
   <option value="site_category:hunting_shooting">Hunting/Shooting</option>
   <option value="site_category:hybrid">Hybrid</option>
   <option value="site_category:ibs_crohns_disease">IBS/Crohn's Disease</option>
   <option value="site_category:illegal_content">Illegal Content</option>
   <option value="site_category:immigration">Immigration</option>
   <option value="site_category:incentivized">Incentivized</option>
   <option value="site_category:incest_abuse_support">Incest/Abuse Support</option>
   <option value="site_category:incontinence">Incontinence</option>
   <option value="site_category:infertility">Infertility</option>
   <option value="site_category:inline_skating">Inline Skating</option>
   <option value="site_category:insurance">Insurance</option>
   <option value="site_category:interior_decorating">Interior Decorating</option>
   <option value="site_category:international_news">International News</option>
   <option value="site_category:internet_technology">Internet Technology</option>
   <option value="site_category:investing">Investing</option>
   <option value="site_category:investors_and_patents">Investors &amp; Patents</option>
   <option value="site_category:islam">Islam</option>
   <option value="site_category:italian_cuisine">Italian Cuisine</option>
   <option value="site_category:italy">Italy</option>
   <option value="site_category:japan">Japan</option>
   <option value="site_category:japanese_cuisine">Japanese Cuisine</option>
   <option value="site_category:java">Java</option>
   <option value="site_category:javascript">JavaScript</option>
   <option value="site_category:jewelry">Jewelry</option>
   <option value="site_category:jewelry_making">Jewelry Making</option>
   <option value="site_category:job_fairs">Job Fairs</option>
   <option value="site_category:job_search">Job Search</option>
   <option value="site_category:judaism">Judaism</option>
   <option value="site_category:k_6_educators">K-6 Educators</option>
   <option value="site_category:landscaping">Landscaping</option>
   <option value="site_category:language_learning">Language Learning</option>
   <option value="site_category:large_animals">Large Animals</option>
   <option value="site_category:latter_day_saints">Latter-Day Saints</option>
   <option value="site_category:law_government_and_politics">Law Government &amp; Politics</option>
   <option value="site_category:legal_issues">Legal Issues</option>
   <option value="site_category:local_news">Local News</option>
   <option value="site_category:logistics">Logistics</option>
   <option value="site_category:luxury">Luxury</option>
   <option value="site_category:mp3_midi">MP3/MIDI</option>
   <option value="site_category:mac_support">Mac Support</option>
   <option value="site_category:magic_and_illusion">Magic &amp; Illusion</option>
   <option value="site_category:marketing">Marketing</option>
   <option value="site_category:marriage">Marriage</option>
   <option value="site_category:martial_arts">Martial Arts</option>
   <option value="site_category:mens_health">Men's Health</option>
   <option value="site_category:metals">Metals</option>
   <option value="site_category:mexican_cuisine">Mexican Cuisine</option>
   <option value="site_category:mexico_and_central_america">Mexico &amp; Central America</option>
   <option value="site_category:minivan">MiniVan</option>
   <option value="site_category:motorcycles">Motorcycles</option>
   <option value="site_category:mountain_biking">Mountain Biking</option>
   <option value="site_category:movies">Movies</option>
   <option value="site_category:music">Music</option>
   <option value="site_category:mutual_funds">Mutual Funds</option>
   <option value="site_category:nascar_racing">NASCAR Racing</option>
   <option value="site_category:national_news">National News</option>
   <option value="site_category:national_parks">National Parks</option>
   <option value="site_category:needlework">Needlework</option>
   <option value="site_category:net_conferencing">Net Conferencing</option>
   <option value="site_category:net_for_beginners">Net for Beginners</option>
   <option value="site_category:hacking">Network Security</option>
   <option value="site_category:media">News</option>
   <option value="site_category:non_standard_content">Non-Standard Content</option>
   <option value="site_category:nursing">Nursing</option>
   <option value="site_category:nutrition">Nutrition</option>
   <option value="site_category:off_road_vehicles">Off-Road Vehicles</option>
   <option value="site_category:olympics">Olympics</option>
   <option value="site_category:options">Options</option>
   <option value="site_category:orthopedics">Orthopedics</option>
   <option value="site_category:pc_support">PC Support</option>
   <option value="site_category:pagan_wiccan">Pagan/Wiccan</option>
   <option value="site_category:paintball">Paintball</option>
   <option value="site_category:painting">Painting</option>
   <option value="site_category:palmtops_pdas">Palmtops/PDAs</option>
   <option value="site_category:panic_anxiety_disorders">Panic/Anxiety Disorders</option>
   <option value="site_category:paranormal_phenomena">Paranormal Phenomena</option>
   <option value="site_category:parenting_k_6_kids">Parenting - K-6 Kids</option>
   <option value="site_category:parenting_teens">Parenting teens</option>
   <option value="site_category:paste">Paste</option>
   <option value="site_category:pediatrics">Pediatrics</option>
   <option value="site_category:performance_vehicles">Performance Vehicles</option>
   <option value="site_category:finance">Personal Finance</option>
   <option value="site_category:pets">Pets</option>
   <option value="site_category:photography">Photography</option>
   <option value="site_category:physical_therapy">Physical Therapy</option>
   <option value="site_category:physics">Physics</option>
   <option value="site_category:pickup">Pickup</option>
   <option value="site_category:politics">Politics</option>
   <option value="site_category:adult">Pornography</option>
   <option value="site_category:portable">Portable</option>
   <option value="site_category:power_and_motorcycles">Power &amp; Motorcycles</option>
   <option value="site_category:pregnancy">Pregnancy</option>
   <option value="site_category:private_school">Private School</option>
   <option value="site_category:pro_basketball">Pro Basketball</option>
   <option value="site_category:pro_ice_hockey">Pro Ice Hockey</option>
   <option value="site_category:adult">Profane Content</option>
   <option value="site_category:psychology_psychiatry">Psychology/Psychiatry</option>
   <option value="site_category:radio">Radio</option>
   <option value="site_category:real_estate">Real Estate</option>
   <option value="site_category:religion">Religion &amp; Spirituality</option>
   <option value="site_category:remodeling_and_construction">Remodeling &amp; Construction</option>
   <option value="site_category:reptiles">Reptiles</option>
   <option value="site_category:resume_writing_advice">Resume Writing/Advice</option>
   <option value="site_category:retirement_planning">Retirement Planning</option>
   <option value="site_category:road_side_assistance">Road-Side Assistance</option>
   <option value="site_category:rodeo">Rodeo</option>
   <option value="site_category:roleplaying_games">Roleplaying Games</option>
   <option value="site_category:rugby">Rugby</option>
   <option value="site_category:running_jogging">Running/Jogging</option>
   <option value="site_category:sailing">Sailing</option>
   <option value="site_category:saltwater_fishing">Saltwater Fishing</option>
   <option value="site_category:scholarships">Scholarships</option>
   <option value="site_category:sci_fi_and_fantasy">Sci-Fi &amp; Fantasy</option>
   <option value="site_category:science">Science</option>
   <option value="site_category:scrapbooking">Scrapbooking</option>
   <option value="site_category:screenwriting">Screenwriting</option>
   <option value="site_category:scuba_diving">Scuba Diving</option>
   <option value="site_category:sedan">Sedan</option>
   <option value="site_category:senior_living">Senior Living</option>
   <option value="site_category:senor_health">Senor Health</option>
   <option value="site_category:sexuality">Sexuality</option>
   <option value="site_category:shareware_freeware">Shareware/Freeware</option>
   <option value="site_category:shopping">Shopping</option>
   <option value="site_category:skateboarding">Skateboarding</option>
   <option value="site_category:skiing">Skiing</option>
   <option value="site_category:sleep_disorders">Sleep Disorders</option>
   <option value="site_category:smoking_cessation">Smoking Cessation</option>
   <option value="site_category:snowboarding">Snowboarding</option>
   <option value="site_category:society">Society</option>
   <option value="site_category:south_america">South America</option>
   <option value="site_category:space_astronomy">Space/Astronomy</option>
   <option value="site_category:spas">Spas</option>
   <option value="site_category:special_education">Special Education</option>
   <option value="site_category:special_needs_kids">Special Needs Kids</option>
   <option value="site_category:sports">Sports</option>
   <option value="site_category:hacking">Spyware/Malware</option>
   <option value="site_category:stamps_and_coins">Stamps &amp; Coins</option>
   <option value="site_category:stocks">Stocks</option>
   <option value="site_category:studying_business">Studying Business</option>
   <option value="site_category:style_and_fashion">Style &amp; Fashion</option>
   <option value="site_category:substance_abuse">Substance Abuse</option>
   <option value="site_category:surfing_bodyboarding">Surfing/Bodyboarding</option>
   <option value="site_category:swimming">Swimming</option>
   <option value="site_category:table_tennis_ping_pong">Table Tennis/Ping-Pong</option>
   <option value="site_category:tax_planning">Tax Planning</option>
   <option value="site_category:tech">Technology &amp; Computing</option>
   <option value="site_category:teens">Teens</option>
   <option value="site_category:telecommuting">Telecommuting</option>
   <option value="site_category:television">Television</option>
   <option value="site_category:tennis">Tennis</option>
   <option value="site_category:theme_parks">Theme Parks</option>
   <option value="site_category:thyroid_disease">Thyroid Disease</option>
   <option value="site_category:travel">Travel</option>
   <option value="site_category:traveling_with_kids">Traveling with Kids</option>
   <option value="site_category:trucks_and_accessories">Trucks &amp; Accessories</option>
   <option value="site_category:us_government_resources">U.S. Government Resources</option>
   <option value="site_category:us_military">U.S. Military</option>
   <option value="site_category:uncategorized">Uncategorized</option>
   <option value="site_category:under_construction">Under Construction</option>
   <option value="site_category:united_kingdom">United Kingdom</option>
   <option value="site_category:unix">Unix</option>
   <option value="site_category:adult">Unmoderated UGC</option>
   <option value="site_category:vegan">Vegan</option>
   <option value="site_category:vegetarian">Vegetarian</option>
   <option value="site_category:veterinary_medicine">Veterinary Medicine</option>
   <option value="site_category:video_and_computer_games">Video &amp; Computer Games</option>
   <option value="site_category:vintage_cars">Vintage Cars</option>
   <option value="site_category:visual_basic">Visual Basic</option>
   <option value="site_category:volleyball">Volleyball</option>
   <option value="site_category:wagon">Wagon</option>
   <option value="site_category:walking">Walking</option>
   <option value="site_category:warez">Warez</option>
   <option value="site_category:waterski_wakeboard">Waterski/Wakeboard</option>
   <option value="site_category:weather">Weather</option>
   <option value="site_category:web_clip_art">Web Clip Art</option>
   <option value="site_category:web_design_html">Web Design/HTML</option>
   <option value="site_category:search_engine">Web Search</option>
   <option value="site_category:weddings">Weddings</option>
   <option value="site_category:weight_loss">Weight Loss</option>
   <option value="site_category:windows">Windows</option>
   <option value="site_category:wine">Wine</option>
   <option value="site_category:womens_health">Women's Health</option>
   <option value="site_category:woodworking">Woodworking</option>
   <option value="site_category:world_soccer">World Soccer</option>

		<?
	}

	


}	