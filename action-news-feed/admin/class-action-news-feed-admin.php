<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       
 * @since      1.0.0
 *
 * @package    Action_news_feed
 * @subpackage Action_news_feed/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Action_news_feed
 * @subpackage Action_news_feed/admin
 * @author     David Baty
 */


if (!class_exists('Action_news_feed_Admin')) {
class Action_news_feed_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $action_news_feed    The ID of this plugin.
	 */
	private $action_news_feed;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;




	/**
	* ==============================================================================	
	* ==============================================================================
	*		
	* Initialize the class and set its properties.
	*
	* @since    1.0.0
	* @param      string    $action_news_feed       The name of this plugin.
	* @param      string    $version    The version of this plugin.
	* ==============================================================================	
	* ==============================================================================		
	*/
	public function __construct( $action_news_feed, $version ) {

		$this->action_news_feed = $action_news_feed;
		$this->version = $version;

		//array of post ID's that this plugin imported
		$this->AnfCreatedPosts = get_option('anf_created_posts');

		//main option -> webhose token
		$this->AnfMainOption = get_option( 'action_news_feed' );

		//number of webhose api requests left
		$this->anfWebhoseReqs = get_option( 'anf_webhose_req_count' );

		$this->anf_allowed_keys = ['id', 'body', 'status', 'query', 'type', 'category', 'schedule_cron', 'post_status', 'import_limit'];

	}//end __construct()





	/*======================================================================================================================
	  ==========================================                           =================================================
	<------------------------------------------|       enqueue assets      |----------------------------------------------->
	============================================                           =================================================
	========================================================================================================================*/


	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* #### Register the stylesheets for the admin area. ####
	*
	* @since    1.0.0
	* ==============================================================================	
	* ==============================================================================		
	*/
	public function enqueue_styles() {

		wp_enqueue_style( $this->action_news_feed, plugin_dir_url( __FILE__ ) . 'css/action-news-feed-admin.css', array(), $this->version, 'all' );

	}//end enqueue_styles()




	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* #### Register the JavaScript for the admin area. ####
	*
	* @since    1.0.0
	* ==============================================================================	
	* ==============================================================================		 
	*/
	public function enqueue_scripts() {

		// in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
		wp_enqueue_script( $this->action_news_feed, plugin_dir_url( __FILE__ ) . 'js/action-news-feed-admin.js', array( 'jquery' ), $this->version, false );


		if (!isset($anf_categories)) $anf_categories = get_categories( array( 'orderby' => 'name', 'hide_empty' => 0, ) );
		$anf_send_cats = json_encode($anf_categories);
		// in JavaScript, object properties are accessed as ajax_object.ajax_url, ajax_object.we_value
		wp_localize_script( $this->action_news_feed, 'ajax_object',  array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'we_value' => $anf_send_cats, 'security'  => wp_create_nonce( 'anf-security-nonce' )));

	}//end enqueue_scripts()




	/*======================================================================================================================
	  ==========================================                           =================================================
	<------------------------------------------|      Welcome Screen     |----------------------------------------------->
	============================================                           =================================================
	========================================================================================================================*/




	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* #### Render the welcom screen. ####	
	* 
	* @since 1.0.0
	* @link  https://codex.wordpress.org/Function_Reference/add_dashboard_page
	* @see   $this->welcome_screen_content()
	*/
	public function welcome_screen_pages() {
  		add_dashboard_page(
    		'Welcome To Action News Feed',
    		'Welcome To Action News Feed',
    		'read',
    		'action-news-feed-welcome',
     		array( $this, 'welcome_screen_content')
  		);
	}



	/**
	* ==============================================================================	
	* ==============================================================================	
	*		
	* #### welcome screen content ####
	*
	* @since 1.0.0
	* @link  https://codex.wordpress.org/Function_Reference/add_dashboard_page
	* @see  $this->welcome_screen_pages()
	*/
	public function welcome_screen_content() {
  		?>
  			
  			<div class="wrap">
    		
    			<h1>Thank You for Action News Feed </h1>

    			<h2>Getting Started</h2>

			<p>t is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>

			<form action='options.php' method='post'>

					<?php
						settings_fields( 'action_news_feed_general');
						//do_settings_sections( 'action_news_feed_general');

						//retrieve the options
						if (!isset($options)) $options = get_option( 'action_news_feed' );
					?>
						<table class="wp-list-table widefat striped">
							<thead>
								<tr>
									<th><h2>Webhose API token <span data-tooltip="This is the API token from webhose. in order to import articles you will have to create a free account and get new token."><i class="dashicons dashicons-info"></i></span></h2></th>
									<th><h2>Featured image placeholder<span data-tooltip="If an article is imported and doesnot have a featured image, the format of the image isnot allowed in wordpress or it is malicous, then this will be teh featured image for that post. if left empty then no featured image will be used for imported posts"><i class="dashicons dashicons-info"></i></span></h2></th>
									<th><h2>Cite reference<span data-tooltip="If you have to or wish to cite the origin author and url on each article you can toggle the option here."><i class="dashicons dashicons-info"></i></span></h2></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<?
											$options['action_news_token'] = empty( $options['action_news_token'] ) ? '' : $options['action_news_token'];
										?>
											<input <?php  echo (NULL == $options['action_news_token'] ||  empty( $options['action_news_token'] ) ? "type='text'" :  "type='password'"); ?> name='action_news_feed[action_news_token]'  value='<?php echo $options['action_news_token']; ?>'>
											<br>
											<p>Dont have a token yet? click <a href="#">Here</a></p>
								</td>
								<td>	
									<?
										//$options = get_option( 'action_news_feed' );
										$options['anf_feat_image_placeholder'] = empty( $options['anf_feat_image_placeholder'] ) ? '' : $options['anf_feat_image_placeholder'];
										
										//get the uploader script
										wp_enqueue_media();
										$placeSrc = wp_get_attachment_url($options['anf_feat_image_placeholder'] );
									?>
										<div class='image-preview-wrapper'>
											<img id='image-preview' src='<?php echo  $placeSrc; ?>' height='100'>
										</div>
										<input id="upload_image_button" type="button" class="button" value="<?php _e( 'Upload image' ); ?>" />
										<input type='hidden' name='action_news_feed[anf_feat_image_placeholder]' id='image_attachment_id' value='<?php echo $options['anf_feat_image_placeholder']; ?>'>
	
								</td>
								<td>

									<?php
										//get origin artic.le url cite option value
										//if (!isset($anf_option_author)) $anf_option_author = get_option( 'action_news_feed' );
											$options['anf_show_origin_author_cite'] = empty( $options['anf_show_origin_author_cite'] ) ? '' : $options['anf_show_origin_author_cite'];

									?>
									<label for="action_news_feed[anf_show_origin_author_cite]">Show origin author cite:
										<input type='checkbox' name='action_news_feed[anf_show_origin_author_cite]' <?php checked( $options['anf_show_origin_author_cite'], 1 ); ?> value='1'>
									</label>				
	
									<br/><br/>
									<?php
										//get origin artic.le url cite option value
										//if (!isset($anf_option_url)) $anf_option_url = get_option( 'action_news_feed' );
											$options['anf_show_origin_url_cite'] = empty( $options['anf_show_origin_url_cite'] ) ? '' : $options['anf_show_origin_url_cite'];

									?>
									<label for="action_news_feed[anf_show_origin_url_cite]">Show origin url cite:
										<input type='checkbox' name='action_news_feed[anf_show_origin_url_cite]' <?php checked( $options['anf_show_origin_url_cite'], 1 ); ?> value='1'>
									</label>
								</td>

							</tr>
							<tr>
								<td>Documentation</td>
								<td>Support</td>
								<td><a href="/admin.php?page=action_news_feed">Go to General Settings Page</a></td>
							</tr>

							</tbody>
							
						</table>


					<?
						submit_button();
					?>

			</form>


  			</div>
  		
  		<?php
	}



	/**
	*
	* ==============================================================================	
	* ==============================================================================	
	*	
	*  #### Redirects to the welcome page only if the transient is set which the transient will only be set on activate ####
	*
	* @since 1.0.0
	* @link  https://codex.wordpress.org/Transients_API
	* @link  https://codex.wordpress.org/Function_Reference/wp_safe_redirect
	* @see  /includes/class-product-quick-view-for-woocommerce-activator.phh -> Product_quick_view_for_woocommerce_Activator::activate();
	*/
	public function welcome_screen_do_activation_redirect() {
  		
  		// Bail if no activation redirect
    	if ( ! get_transient( '_action_news_feed_welcome_screen_activation_redirect' ) ) {
    		return;
  		}

  		// Delete the redirect transient
  		delete_transient( '_action_news_feed_welcome_screen_activation_redirect' );

  		// Bail if activating from network, or bulk
  		if ( is_network_admin() || isset( $_GET['activate-multi'] ) ) {
    		return;
  		}

  		// Redirect to bbPress about page
  		wp_safe_redirect( add_query_arg( array( 'page' => 'action-news-feed-welcome' ), admin_url( 'index.php' ) ) );

	}


	/**
	* ==============================================================================	
	* ==============================================================================	
	*		
	* #### remove the dashboard menu page if it exists ####
	*
	* @since 1.0.0
	*/
	public function welcome_screen_remove_menus() {
    	remove_submenu_page( 'index.php', 'action-news-feed-welcome' );
	}







	/**
	* ==============================================================================	
	* ==============================================================================	
	*
	* #### run shcduel now from ajax ####
	*
	* @since 1.0.0
	*
	* ==============================================================================	
	* ==============================================================================
	**/
	public function anf_run_schedule_now() {

    	ob_start();

    		//verify nonce
    		if ( ! check_ajax_referer( 'anf-security-nonce', 'security' ) ) {

				$return = array('message' => "Invalid security token sent");

	         	wp_send_json_error($return);

    			wp_die();
  			} 

  			//verify user
  			if ( current_user_can( 'manage_options' ) ) {

  				if (!isset($anfRows)) $anfRows = $_POST['anf_rows'];

   				wp_cache_delete ( 'alloptions', 'options' );

   				if (!isset($anf_option_name)) $anf_option_name = 'anf_schedule_rows_opts';
				if (!isset($new_value)) $new_value = 0;

					if ( get_option( $anf_option_name ) !== false ) {

						$anf_schedule_opt = get_option('anf_schedule_rows_opts');

						//keep row id unique
						if (!isset($anfRandID)) $anfRandID = $this->generateRandomString();

							if(!in_array( $anfRandID, $anf_schedule_opt) ) { 

					   			$anf_arr_create = array(
					        		'id' => $anfRandID, 
					        		'body' =>  array(
                                		'status' => 0,
								 		'query' => '',
								 		'type' => 'post',
								 		'category' => 'Uncategorised',
								 		'schedule_cron' => '',
								 		'post_status' => 'draft',
								 		'import_limit' => '3'
									)
					   	 		);


					   			//filter the above array to use only allowed keys
					   			$this->anf_force_arr_keys($anf_arr_create, $this->$this->$anf_allowed_keys);





							} 

					array_push( $anf_schedule_opt, $anf_arr_create );

    				update_option( $anf_option_name, $anf_schedule_opt );

				} else {

					$anf_schedule_opt = array(
					    array(
					        'id' => '123', 
					        'body' =>  array(
                                	'status' => 0,
								 	'query' => 'test',
								 	'type' => 'post',
								 	'category' => 'Uncategorised',
								 	'schedule_cron' => '',
								 	'post_status' => 'draft',
								 	'import_limit' => '3'
							)
					    ),
					);					


					//filter the above array to use only allowed keys
					$this->anf_force_arr_keys($anf_schedule_opt, $this->$this->$anf_allowed_keys);



					if (!isset($anf_deprecated)) $anf_deprecated = null;
					if (!isset($anf_autoload)) $anf_autoload ='no';
    				add_option( $anf_option_name, $anf_schedule_opt, $anf_deprecated, $anf_autoload );

    				echo 'added option';
				
				}

			print json_encode($anf_arr_create);

		} else {
    		//send to eror log
    		echo "Do not have permision";
    		wp_die();
		}


  		$response = ob_get_contents();
        ob_end_clean();
        echo $response;
        die(1);
    }//end 	anf_run_schedule_now






    /**
	* ==============================================================================	
	* ==============================================================================
	*    
    * delete all posts created by this plugin
    *
    * @since 1.0.0
	* ==============================================================================	
	* ==============================================================================
    **/
	public function anf_reset_schedule() {

    	ob_start();

    	//verify nonce
		if ( ! check_ajax_referer( 'anf-security-nonce', 'security' ) ) {

			$return = array('message' => "Invalid security token sent");

	         wp_send_json_error($return);

    		wp_die();
  		}  

  		if ( current_user_can( 'manage_options' ) ) {

  		if (!isset($whatever)) $whatever = $_POST['resetIt'];

			delete_option('anf_schedule_rows_opts');

				wp_cache_delete ( 'alloptions', 'options' );

					if (!isset($anf_option_name_rows)) $anf_option_name_rows = 'anf_schedule_rows_opts';
					if (!isset($new_value)) $new_value = 0;

						if ( get_option( $anf_option_name_rows ) == false ) {

							if (!isset($anf_arr_two_append)) $anf_arr_two_append =array(
					    		array(
					        		'id' => '123', 
					        		'body' =>  array(
                                				'status' => 0,
								 				'query' => 'performance_score:>5 spam_score:<.8 site_category:sports language:english site_type:news',
								 				'type' => 'post',
								 				'category' => 'Uncategorised',
								 				'schedule_cron' => '',
								 				'post_status' => 'draft',
								 				'import_limit' => '3'
									)
					    		),   
							);

							//filter the above array to use only allowed keys
					   		$this->anf_force_arr_keys($anf_arr_two_append, $this->$this->$anf_allowed_keys);

						}

					if (!isset($anf_deprecated)) $anf_deprecated = null;
					if (!isset($anf_autoload)) $anf_autoload  = 'no';
    				add_option( $anf_option_name_rows, $anf_arr_two_append, $anf_deprecated, $anf_autoload );

    				echo 'Reset succesfully';
    	} else {
    		//send to eror log
    		echo "Do not have permision";
    		wp_die();
    	}

  		$response =ob_get_contents();
        ob_end_clean();
        echo $response;
        die(1);
    }// end anf_reset_schedule()




    /**
	* ==============================================================================	
	* ==============================================================================
	*    
    * delete all posts created by this plugin
    *
    * @since 1.0.0
	* ==============================================================================	
	* ==============================================================================
    **/
    public function anf_delete_created_posts() {

    	ob_start();

    	//verify nonce
		if ( ! check_ajax_referer( 'anf-security-nonce', 'security' ) ) {

			$return = array('message' => "Invalid security token sent");

	         wp_send_json_error($return);

    		wp_die();
  		}  

  		if ( current_user_can( 'manage_options' ) ) {
		//$anfTracker = get_option('anf_created_posts');

		foreach ($this->AnfCreatedPosts as $key => $postid) {

			//ddelete the attachment
			 if(in_array( $postid, $this->AnfCreatedPosts)) {
    			if(has_post_thumbnail( $post_id_alt )) {
      				$attachment_id = get_post_thumbnail_id( $post_id_alt );
      				wp_delete_attachment($attachment_id, true);
    			}
			}

			$post = get_post((int)$postid);

			if(empty($post)) {
				return;
			}

			//delete it
			wp_delete_post( $postid, true ); 


		}

		if (!isset($anf_new_value_arr)) $anf_new_value_arr = array( );
		update_option( 'anf_created_posts', $anf_new_value_arr  );


		echo "All Posts Deleted";

	} else  {
		//send to eror log
    	echo "Do not have permision";
    	wp_die();
	}


  		$response =ob_get_contents();
        ob_end_clean();
        echo $response;
        die(1);
    }//end anf_delete_created_posts()







	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* delete schedule
	*
	* @since 1.0.0
	* ==============================================================================	
	* ==============================================================================	
	*/
	public function anf_delete_schudule() {

    	ob_start();

    	//verify nonce
    	if ( ! check_ajax_referer( 'anf-security-nonce', 'security' ) ) {

			$return = array('message' => "Invalid security token sent");

	         wp_send_json_error($return);

    		wp_die();
  		}  

  		//verify user
  		if ( current_user_can( 'manage_options' ) ) {

  			if (!isset($anf_sched_id)) $anf_sched_id = $_POST['delete_id'];
  			if (!isset($anf_schedule_option)) $anf_schedule_option = 'anf_schedule_rows_opts';
  			if (!isset($anf_arr_to_append)) $anf_arr_to_append = get_option('anf_schedule_rows_opts');
             
             	//remove the delted row from the array
   				$anf_remove_id = $this->removeElementWithValue($anf_arr_to_append, "id", $anf_sched_id);

				$anf_new_arr = $anf_remove_id;

    		update_option( $anf_schedule_option, $anf_new_arr );
 
    		print_r($anf_new_arr);

		} else  {
			//send to eror log
    		echo "Do not have permision";
    		wp_die();
		}

  		$response = ob_get_contents();
        ob_end_clean();
        echo $response;
        die(1);
    }//end 	anf_delete_schudule()



	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* 
	*
	* @since 1.0.0
	* ==============================================================================	
	* ==============================================================================	
	*/
    public function anf_save_schedule() {

    	ob_start();

		if ( ! check_ajax_referer( 'anf-security-nonce', 'security' ) ) {

			$return = array('message' => "Invalid security token sent");

	         wp_send_json_error($return);

    		wp_die();
  		}     

  		/*here*/ 	
  		//verify user
  		if ( current_user_can( 'manage_options' ) ) {
    	$whatever = $_POST['whatever'];

    	//remove bullshit
		unset($whatever['option_page'], $whatever['action'], $whatever['_wpnonce'], $whatever['_wp_http_referer'], $whatever['anf_schedule_rows_opts['.$c.'']);

		$blankArr = array();
			
			foreach ($whatever as $value) {

					   $t = array(
					        'id' => $value['id'], 
					        'body' =>  array(
                                	'status' => $value['status'],
								 	'query' => $value['query'],
								 	'type' => $value['type'],
								 	'category' => $value['category'],
								 	'schedule_cron' => $value['schedule_cron'],
								 	'post_status' => $value['post_status'],
								 	'import_limit' => $value['import_limit']
							)
					    );

					   	//filter the above array to use only allowed keys
					   	$this->anf_force_arr_keys($t, $this->$this->$anf_allowed_keys);

					   array_push($blankArr, $t);
			}


			$option_name = 'anf_schedule_rows_opts';

		
			update_option( $option_name, $blankArr );


			$this->anf_schedule_crons();
		}

  		$response =ob_get_contents();
        ob_end_clean();
        echo $response;
        die(1);    	
    }// end anf_save_schedule()


	/*======================================================================================================================
	  ==========================================                           =================================================
	<------------------------------------------|      Helper Functions     |----------------------------------------------->
	============================================                           =================================================
	========================================================================================================================*/


	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* Check user role 
	*
	* @since 1.0.0
	* @example if($this->anf_check_user_role() == 1 ) {
	* @example if( Product_quick_view_for_woocommerce::anf_check_user_role()  == 1 ) { echo 'string';}
	* ==============================================================================	
	* ==============================================================================		
	*/
	public function anf_check_user_role( $arr ) {

		foreach ($arr as $wcqv_cap ) {
			if( current_user_can( $wcqv_cap ) ) {
				return true;

			} else {
				return false;
			}
		}

	}//end anf_check_user_role()




	/**
	* ==============================================================================	
	* ==============================================================================	
	*
	* fetch categoreis helper function
	*
	* @since 1.0.0
	*
	* ==============================================================================	
	* ==============================================================================	
	*/
	private function anf_category_fetch_helper() {

		$categories = get_categories( array( 'orderby' => 'name', 'hide_empty' => 0, ) );
 			
 			?>
				<select name="" id="">
			<?
					foreach ( $categories as $category ) {
    					?>
							<option value="<?php echo $category->name; ?>"><?php echo $category->name; ?></option>
						<?
					}
			?>
				</select>
			<?php
	}//end anf_category_fetch_helper()




    /**
	* ==============================================================================	
	* ==============================================================================
	*	    
    *
    *
    * @since 1.0.0
	* ==============================================================================	
	* ==============================================================================	
    */
	private function generateRandomString($length = 10) {
    	$characters = '0123456789';
    	$charactersLength = strlen($characters);
    	$randomString = '';
    		for ($i = 0; $i < $length; $i++) {
        		$randomString .= $characters[rand(0, $charactersLength - 1)];
    		}
    	return $randomString;
	}//end generateRandomString()




    /**
	* ==============================================================================	
	* ==============================================================================
	*	    
    * removes row from array
    *
    * @since 1.0.0
	* ==============================================================================	
	* ==============================================================================	
    */
	private function removeElementWithValue($array, $key, $value){
     	foreach($array as $subKey => $subArray){
          if($subArray[$key] == $value){
               unset($array[$subKey]);
          }
     	}
     return $array;
	}//end  removeElementWithValue()



    /**
	* ==============================================================================	
	* ==============================================================================
	*	    
    * 
    *
    * @since 1.0.0
	* ==============================================================================	
	* ==============================================================================	
    */
	private function anf_force_arr_keys($arr, $allowed) {							
	
		foreach ($arr as $key2 => $value) {
			if(!in_array($key2, $allowed)) {
    			unset($arr[$key2]);
		}

			if(is_array($value)) {
				foreach (array_keys($value) as $key) {
  					if(!in_array($key, $allowed)) {
       					unset($arr[$key2][$key]);
					}
				}
			}
		}
		//return teh filtered array
		return $arr;

	}


	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* Progromaticlay inert featured image when using wp_insert_post().  This function takes in the img url and post id, then creates the file and sets it as the featured image.
	*
	* @since 1.0.0
	* @link        https://wordpress.stackexchange.com/questions/40301/how-do-i-set-a-featured-image-thumbnail-by-image-url-when-using-wp-insert-post/41300
	* @link        https://developer.wordpress.org/reference/functions/wp_upload_dir/
	* @link        https://codex.wordpress.org/Function_Reference/wp_insert_attachment
	* @link        https://codex.wordpress.org/Function_Reference/wp_mkdir_p
	* @param       string     required      $image_url    Url of the image to import as feat. image
	* @param 	   bool       requiired     $post_id      id of post created by wp_insert_post())
	* @example     $this->Generate_Featured_Image( 'IMAGE_URL_HERE',   $post_id );
	* ==============================================================================	
	* ==============================================================================		
	**/
	function Generate_Featured_Image( $image_url, $post_id  ){
    	$upload_dir = wp_upload_dir();
    	$image_data = file_get_contents($image_url);
    	$filename = basename($image_url);
    		
    		if(wp_mkdir_p($upload_dir['path']))     $file = $upload_dir['path'] . '/' . $filename;
    		else                                    $file = $upload_dir['basedir'] . '/' . $filename;
    			file_put_contents($file, $image_data);

    	$wp_filetype = wp_check_filetype($filename, null );
    	$attachment = array(
        	'post_mime_type' => $wp_filetype['type'],
        	'post_title' => sanitize_file_name($filename),
        	'post_content' => '',
        	'post_status' => 'inherit'
    	);
    	$attach_id = wp_insert_attachment( $attachment, $file, $post_id );
    	
    	require_once(ABSPATH . 'wp-admin/includes/image.php');
    	$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    	$res1= wp_update_attachment_metadata( $attach_id, $attach_data );
    	$res2= set_post_thumbnail( $post_id, $attach_id );
	}//end Generate_Featured_Image()





	/*======================================================================================================================
	  ==========================================                           =================================================
	<------------------------------------------|  Settings options & tabs  |----------------------------------------------->
	============================================                           =================================================
	========================================================================================================================*/




    /**
	* ==============================================================================	
	* ==============================================================================	
	*
	*
	*    
    * @since 1.0.0
    * @link https://developer.wordpress.org/reference/functions/add_submenu_page/
    * @param   string    required   $parent_slug   The slug name for the parent menu (or the file name of a standard WordPress admin page
    * @param   string    required   $page_title    The text to be displayed in the title tags of the page when the menu is selected.
    * @param   string    required   $menu_title    The text to be used for the menu.
    * @param   string    required   $menu_slug     The slug name to refer to this menu by
    * @param   callable  optional   $function      he function to be called to output the content for this page.
	* ==============================================================================	
	* ==============================================================================	    
    */
	public function action_news_feed_add_admin_menu() { 


		//add_submenu_page( 'woocommerce', 'Quick view', 'Quick view', 'manage_options', 'action_news_feed',   array( $this, 'action_news_feed_options_page' )); 

		if($this->anf_check_user_role( array('manage_options') ) == true ) { 

				add_menu_page( 'Action News Feed', 'Action News Feed', 'manage_options', 'action_news_feed',  array( $this, 'anf_admin_settings_page' ),'dashicons-video-alt', 150);
		}

	}//end action_news_feed_add_admin_menu()




	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* register settings section and fields
	*
	* @since 1.0.0
	*
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================	
	**/
	public function action_news_feed_settings_init() { 
		
		register_setting( 
			'action_news_feed_general', // Option group
			'action_news_feed',// Option name
			array( $this, 'anf_sanitize' ) // Sanitize
		);

			add_settings_section(
				'action_news_feed_section', 
				__( 'General', 'action_news_feed' ), 
				array( $this, 'action_news_feed_section_callback'), 
				'action_news_feed_general'
			);
				add_settings_field(//webhose token
					'action_news_token',
					__('webhose.io token key<br/> "Dont have one? <a href="https://webhose.io"/>Click Here</a>'),
					array( $this, 'action_news_token_render' ), 
					'action_news_feed_general', 
					'action_news_feed_section' 			
				);		
				add_settings_field(//feat image placeholder
					'anf_feat_image_placeholder',
					__('placeholder'),
					array( $this, 'anf_feat_img_placeholder_render' ), 
					'action_news_feed_general', 
					'action_news_feed_section' 			
				);								
				add_settings_field(//show orrigin auth cite
					'anf_show_origin_author_cite',
					__('Show the origin author cite at the bottom of each post'),
					array( $this, 'anf_show_origin_author_render' ), 
					'action_news_feed_general', 
					'action_news_feed_section' 			
				);	
				add_settings_field(//show orrigin auth cite
					'anf_show_origin_url_cite',
					__('Show the origin url cite at the bottom of each post'),
					array( $this, 'anf_show_origin_url_render' ), 
					'action_news_feed_general', 
					'action_news_feed_section' 			
				);					

	}//action_news_feed_settings_init	




	/**
	* ==============================================================================	
	* ==============================================================================		
	* set up the settings page tabs
	* @since 1.0.0
	* @see function action_news_feed_add_admin_menu()
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================	
	**/
	public function anf_admin_settings_page(){
		global $sd_active_tab;
		$sd_active_tab = isset( $_GET['tab'] ) ? $_GET['tab'] : 'anfGeneralSettings'; ?>
 
 			<h1><?php _e( 'Action News Feed', 'action_news_feed' ); ?></h1>
				<h2 class="nav-tab-wrapper">
					<?php
						do_action( 'anf_settings_tab' );
					?>
				</h2>
					<?php
						do_action( 'anf_settings_content' );
	}//end anf_admin_settings_page()




	/**
	* ==============================================================================	
	* ==============================================================================		
	*
	* set up general tab url
	*
	* @since 1.0.0
	*
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================		
	**/
	public function anf_general_tab_init(){
		global $sd_active_tab; ?>
			<a class="nav-tab <?php echo $sd_active_tab == 'anfGeneralSettings' || '' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'admin.php?page=action_news_feed&tab=anfGeneralSettings' ); ?>"><?php _e( 'General', 'action_news_feed' ); ?> </a>
		<?php
	}//end anf_general_tab_init()






	public function anf_print_media_uploader_scripts() {
	//$my_saved_attachment_post_id = get_option( 'anf_feat_image_placeholder', 0 );
	$options = get_option( 'action_news_feed' );
	$my_saved_attachment_post_id = $options['anf_feat_image_placeholder'];
	?><script type='text/javascript'>
		jQuery( document ).ready( function( $ ) {
			// Uploading files
			var file_frame;
			var wp_media_post_id = wp.media.model.settings.post.id; // Store the old id
			var set_to_post_id = <?php echo $my_saved_attachment_post_id; ?>; // Set this
			jQuery('#upload_image_button').on('click', function( event ){
				event.preventDefault();
				// If the media frame already exists, reopen it.
				if ( file_frame ) {
					// Set the post ID to what we want
					file_frame.uploader.uploader.param( 'post_id', set_to_post_id );
					// Open frame
					file_frame.open();
					return;
				} else {
					// Set the wp.media post id so the uploader grabs the ID we want when initialised
					wp.media.model.settings.post.id = set_to_post_id;
				}
				// Create the media frame.
				file_frame = wp.media.frames.file_frame = wp.media({
					title: 'Select a image to upload',
					button: {
						text: 'Use this image',
					},
					multiple: false	// Set to true to allow multiple files to be selected
				});
				// When an image is selected, run a callback.
				file_frame.on( 'select', function() {
					// We set multiple to false so only get one image from the uploader
					attachment = file_frame.state().get('selection').first().toJSON();
					// Do something with attachment.id and/or attachment.url here
					$( '#image-preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
					$( '#image_attachment_id' ).val( attachment.id );
					// Restore the main post ID
					wp.media.model.settings.post.id = wp_media_post_id;
				});
					// Finally, open the modal
					file_frame.open();
			});
			// Restore the main ID when the add media button is pressed
			jQuery( 'a.add_media' ).on( 'click', function() {
				wp.media.model.settings.post.id = wp_media_post_id;
			});
		});
	</script><?php
	}




	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* general options tab content/settings
	*
	* @since 1.0.0
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================	
	**/
	public function anf_general_render_options_tab() {
		global $sd_active_tab;
			if ( '' || 'anfGeneralSettings' != $sd_active_tab )
				return;



		if($this->anf_check_user_role( array('manage_options') ) == true ) { 

		?>


			<form action='options.php' method='post'>

					<?php
						settings_fields( 'action_news_feed_general');
						//do_settings_sections( 'action_news_feed_general');

						//retrieve the options
						if (!isset($options)) $options = get_option( 'action_news_feed' );
					?>
						<table class="wp-list-table widefat striped">
							<thead>
								<tr>
									<th><h2>Webhose API token <span data-tooltip="This is the API token from webhose. in order to import articles you will have to create a free account and get new token."><i class="dashicons dashicons-info"></i></span></h2></th>
									<th><h2>Featured image placeholder<span data-tooltip="If an article is imported and doesnot have a featured image, the format of the image isnot allowed in wordpress or it is malicous, then this will be teh featured image for that post. if left empty then no featured image will be used for imported posts"><i class="dashicons dashicons-info"></i></span></h2></th>
									<th><h2>Cite reference<span data-tooltip="If you have to or wish to cite the origin author and url on each article you can toggle the option here."><i class="dashicons dashicons-info"></i></span></h2></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<?
											$options['action_news_token'] = empty( $options['action_news_token'] ) ? '' : $options['action_news_token'];
										?>
											<input <?php  echo (NULL == $options['action_news_token'] ||  empty( $options['action_news_token'] ) ? "type='text'" :  "type='password'"); ?> name='action_news_feed[action_news_token]'  value='<?php echo $options['action_news_token']; ?>'>
											<br>
											<p>Dont have a token yet? click <a href="#">Here</a></p>
								</td>
								<td>	
									<?
										//$options = get_option( 'action_news_feed' );
										$options['anf_feat_image_placeholder'] = empty( $options['anf_feat_image_placeholder'] ) ? '' : $options['anf_feat_image_placeholder'];
										
										//get the uploader script
										wp_enqueue_media();
										$placeSrc = wp_get_attachment_url($options['anf_feat_image_placeholder'] );
									?>
										<div class='image-preview-wrapper'>
											<img id='image-preview' src='<?php echo  $placeSrc; ?>' height='100'>
										</div>
										<input id="upload_image_button" type="button" class="button" value="<?php _e( 'Upload image' ); ?>" />
										<input type='hidden' name='action_news_feed[anf_feat_image_placeholder]' id='image_attachment_id' value='<?php echo $options['anf_feat_image_placeholder']; ?>'>
	
								</td>
								<td>

									<?php
										//get origin artic.le url cite option value
										//if (!isset($anf_option_author)) $anf_option_author = get_option( 'action_news_feed' );
											$options['anf_show_origin_author_cite'] = empty( $options['anf_show_origin_author_cite'] ) ? '' : $options['anf_show_origin_author_cite'];

									?>
									<label for="action_news_feed[anf_show_origin_author_cite]">Show origin author cite:
										<input type='checkbox' name='action_news_feed[anf_show_origin_author_cite]' <?php checked( $options['anf_show_origin_author_cite'], 1 ); ?> value='1'>
									</label>				
	
									<br/><br/>
									<?php
										//get origin artic.le url cite option value
										//if (!isset($anf_option_url)) $anf_option_url = get_option( 'action_news_feed' );
											$options['anf_show_origin_url_cite'] = empty( $options['anf_show_origin_url_cite'] ) ? '' : $options['anf_show_origin_url_cite'];

									?>
									<label for="action_news_feed[anf_show_origin_url_cite]">Show origin url cite:
										<input type='checkbox' name='action_news_feed[anf_show_origin_url_cite]' <?php checked( $options['anf_show_origin_url_cite'], 1 ); ?> value='1'>
									</label>
								</td>

							</tr>

							</tbody>
							
						</table>


					<?
						submit_button();
					?>

			</form>


			<br>

			
			<table class="wp-list-table widefat striped">
	
				<tbody>
					<tr>
						<td>
							<h2>Delete all posts created by this plugin<span data-tooltip="This will delete all posts ceated by this plugin. I reccomend you do a backup first as this is not reversable"><i class="dashicons dashicons-info"></i></span></h2>
						</td>
						<td>
							<h2>Number Webhose requests left <span data-tooltip="Number of webhose api requests left this month"><i class="dashicons dashicons-info"></i></span></h2>
						</td>
					</tr>
					<tr>
						<td>
							<span style="float: left;"><button onclick="anf_delete_created_posts_js()">Delete Now</button></span>
							<span style="float: left;" id="anf_deleteAll"><div class="anfStatusCheck"></div><div class="anfspinner"></div></span>
						</td>
						<td>
							<?php
								echo $this->anfWebhoseReqs;
							?>
						</td>
					</tr>
				</tbody>
			</table>
			<br>
				<h1>Quick Import</h1>
			
					<table id="anf_quick_import_tbl" class="wp-list-table widefat striped">
						<tr>
							<th>Search Term <span data-tooltip="This is your what you want to return. you have an array of things you can input, anything from filters to excluding terms to posts that haev x ampount of likes on facebook.  reference the search query tab on this page for assistance."><i class="dashicons dashicons-info"></i></span></th>
							<th>Type <span data-tooltip="this is what you want the imported articles to be imported as, a post or a page"><i class="dashicons dashicons-info"></i></span></th>
							<th>Category <span data-tooltip="Choose from the avaiable categories.  these are the categories you create in wordpress.  You can add more categories by going to the catergories section under posts and add a new category."><i class="dashicons dashicons-info"></i></span></th>
							<th>Number to import <span data-tooltip="this is the number of articles that you want to import"><i class="dashicons dashicons-info"></i></span></th>
							<th>post status <span data-tooltip="this is what you want the status of the imported articles to be once the are imported.  I reccomend you leave them as a draft so you have a chance to review the articles before they are published for the public to see."><i class="dashicons dashicons-info"></i></span></th>
							<th>Status <span data-tooltip="this is the status of the import, if an error does show it will be here"><i class="dashicons dashicons-info"></i></span></th>
							<th>Run <span data-tooltip="click to run the import"><i class="dashicons dashicons-info"></i></span></th>
						</tr>
						<tr>
							<td><input id="anfQuickImportQuery" type="text" placeholder="EX: Tesla Motors" />
								<span title="Query Helper" onclick="anf_trigger_modal_quick()" class="dashicons dashicons-plus"></span> <span>Query Builder</span>
<div class="quikQueryopts">
									

<div id="api-add-menu-key" class="qb-card qb-menu" style="">

                                <div class="qb-menu-heading">
                                    Add Filter
                                    <a href="https://docs.webhose.io/docs/filters-reference" target="_blank" style="float: right; margin-right: 10px;">Filters References</a>
                                    <p onclick="anf_closequikQueryopts()" class="closequikQueryopts"> X </p>
                                </div>

                                <div style="text-align: left">
                                        <input type="text" placeholder="Search Filter" id="anf_search" class="filter-the-filters">
                                </div>
                                <div id="params-scroll">
                                	<div id="results"></div>


                                </div>
                                
                            </div>
                          <button onclick="anf_closequikQueryopts()"> Close </button>
							

	</div>
							<td>
								<select name="anfImportType" id="anfImportType">
									<option value="post">post</option>
									<option value="page">page</option>
								</select>
							</td>
							<td>
								<?php
									$categories = get_categories( array( 'orderby' => 'name', 'hide_empty' => 0, ) );
 								?>
									<select name="anfCategoryType" id="anfCategoryType">
										<?
											foreach ( $categories as $category ) {
    									?>
												<option value="<?php echo $category->term_id; ?>"><?php echo $category->name; ?></option>
										<?
											}
										?>
									</select>

							</td>
							<td>
								<input  value="4" id="anfNumber2import" name="anfNumber2import" type="number" placeholder="default is 10">
							</td>

							<td>
								<select name="anfImportStatus" id="anfImportStatus">
									<option value="draft">draft</option>
									<option value="publish">publish</option>
								</select>								
							</td>
														<td>
								<div class="anfStatusCheck"></div>
								<div class="anfspinner"></div>
							</td>
							<td>
								<button onclick="anfQuickImport()" id="anfimportNow">
									Import Now
								</button>
							</td>
						</tr>
					</table>	
		
					<!--Results table-->
					<table id="resultTable" class="wp-list-table widefat striped">
						<thead id="daveresult">
							<tr>
								<th>Title</th>
     							<th>Image</th>
     							<th>content</th>
							</tr>
						</thead>
					</table>
		<?php
		}	
	}//end anf_general_render_options_tab()




	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* set up advanced options tab url
	*
	* @since 1.0.0
	*
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================		
	**/
	public function anf_advanced_tab_init(){
		global $sd_active_tab; ?>
			<a class="nav-tab <?php echo $sd_active_tab == 'wcqv-advanced-settings' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'admin.php?page=action_news_feed&tab=wcqv-advanced-settings' ); ?>"><?php _e( 'Schedule Settings', 'action_news_feed' ); ?> </a>
		<?php
	}//end anf_advanced_tab_init()
 










	/**
	* ==============================================================================	
	* ==============================================================================
	*	
	* advanced options tab content
	*
	* @since 1.0.0
	*
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================	
	**/ 
	public function anf_advanced_tab_tab() {

		global $sd_active_tab;

			if ( 'wcqv-advanced-settings' != $sd_active_tab )
				return;

			if($this->anf_check_user_role( array('manage_options') ) == true ) { 
		?>

			<form id="scheduleOptionsForm" action='options.php' method='post'>

				<?php

					settings_fields( 'anf_schedule_imports');
					do_settings_sections( 'anf_schedule_imports');

				?>

					<h1>scheduled imports</h1>
					<button class="button-primary" onclick="anfRunScheduleNow(event)"><span>Add Row</span></button>

				<?

					$r = get_option('anf_schedule_rows_opts');

					//print_r($r);
					//echo '<pre>'; print_r( _get_cron_array() ); echo '</pre>';

					if( $r ) {

				?>

					<div style="float: right; text-align: right; padding-right: 10px; margin-bottom: 10px;"><h3>reset schedules</h3> <p><em><strong>Note: </strong> this will delete all saved schedules</em></p><button onclick="anf_reset_schedule_js(event)">Reset</button></div>
							<table class="wp-list-table widefat striped" id="ddbody">
								<thead>
									<tr>
										<th>active <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
     									<th>Search query <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
        								<th>type <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
        								<th>Category <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
        								<th>Import limit <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
        								<th>post/page Status <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
        								<th>Schedule (hrs) <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
        								<th>Status <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
        								<th>run <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
        								<th>delete <span data-tooltip=""><i class="dashicons dashicons-info"></i></span></th>
									</tr>
								</thead>
						<tbody id="anf-schedule-result">
								<?php

									
									$c = 0;
									foreach ($r as $rr) {
										 ?><tr id="<?php echo $rr['id']; ?>"><?
										 	foreach ($rr as  $t2) {
										 		if(is_array($t2)) {
										 			?>
														<td>
															<?php $t2['status'] = empty( $t2['status'] ) ? 0 : 1; ?>
															<input type='hidden' name='anf_schedule_rows_opts[<?php echo $rr['id']; ?>][id]'  value='<?php echo $rr['id']; ?>'>

															<input type='checkbox' name='anf_schedule_rows_opts[<?php echo  $rr['id']; ?>][status]' <?php checked( $t2['status'], 1 ); ?> value='1'/>
														</td>
														<td>
															<?php $t2['query'] = empty( $t2['query'] ) ? '' : $t2['query']; ?>
																<input id="anf_schedule_input_query_<?php echo $rr['id']; ?>" class="anf_schedule_input_query" type='text' placeholder="query" name='anf_schedule_rows_opts[<?php echo $rr['id'] ?>][query]'  value='<?php echo $t2['query']; ?>'/><span title="Query Helper" onclick="anf_trigger_modal(<?php echo $rr['id']; ?>)" class="dashicons dashicons-plus"></span>
																	<select id="api-add-param-<?php echo $rr['id']; ?>" style="display: none">
																		<option value="site_category:" disabled selected>Select your option</option>
																			<?php
																				$t = new Action_news_feed_query_builder_parts();
																				echo $t->anf_query_select_site_category();
																			?>
																	</select>
														</td>
														<td>
															<select class="anf_schedule_input_type" name="anf_schedule_rows_opts[<?php echo $rr['id'] ?>][type]" id="dropdown_option_0">
            													<?php $selected = (isset( $t2['type']  ) && $t2['type']  === 'post') ? 'selected' : '' ; ?>
            														<option value="post" <?php echo $selected; ?>>post</option>
            													<?php $selected = (isset( $t2['type']  ) && $t2['type']  === 'page') ? 'selected' : '' ; ?>
            														<option value="page" <?php echo $selected; ?>>page</option>
        													</select>	
														</td>
														<td>
															<?php 

																$t2['category'] = empty( $t2['category'] ) ? '' : $t2['category'];

																$categories = get_categories( array( 'orderby' => 'name', 'hide_empty' => 0, ) );

 															?>
																<select class="anf_schedule_input_category" name="anf_schedule_rows_opts[<?php echo $rr['id']; ?>][category]" id="dropdown_option_0">
															<?
														
																foreach ( $categories as $category ) {
    														
    																$selected = (isset($t2['category']  ) && $t2['category']  === ''.$category->term_id.'') ? 'selected' : '' ; ?>
            				
            															<option value="<?php echo $category->term_id; ?>" <?php echo $selected; ?>><?php echo $category->name; ?></option>

															<?
																}
															?>
																</select>

														</td>
														<td>
															<?php $t2['import_limit'] = empty( $t2['import_limit'] ) ? '' : $t2['import_limit']; ?>

																<input class="anf_schedule_input_import_limit" type='number' placeholder="default is 10" name='anf_schedule_rows_opts[<?php echo $rr['id']; ?>][import_limit]'  value='<?php echo $t2['import_limit']; ?>' />
														</td>														
														<td>
        													<select class="anf_schedule_input_post_status" name="anf_schedule_rows_opts[<?php echo $rr['id']; ?>][post_status]" id="dropdown_option_0">
            													<?php $selected = (isset( $t2['post_status']  ) && $t2['post_status']  === 'draft') ? 'selected' : '' ; ?>
            														<option value="draft" <?php echo $selected; ?>>draft</option>
            													<?php $selected = (isset( $t2['post_status']  ) && $t2['post_status']  === 'published') ? 'selected' : '' ; ?>
            														<option value="published" <?php echo $selected; ?>>published</option>
        													</select>											
														</td>
											<td>
												<?php  $t2['schedule_cron'] = empty( $t2['schedule_cron'] ) ? '' : $t2['schedule_cron']; ?>

													

													<select  class="anf_schedule_input_schedule_cron" name="anf_schedule_rows_opts[<?php echo $rr['id']; ?>][schedule_cron]" id="dropdown_option_0">
            											<?php $selected = (isset( $t2['schedule_cron']  ) && $t2['schedule_cron']  === 'daily') ? 'selected' : '' ; ?>
            												<option value="daily" <?php echo $selected; ?>>daily</option>
            											<?php $selected = (isset( $t2['schedule_cron']  ) && $t2['schedule_cron']  === 'weekly') ? 'selected' : '' ; ?>
            												<option value="weekly" <?php echo $selected; ?>>weekly</option>
            											<?php $selected = (isset( $t2['schedule_cron']  ) && $t2['schedule_cron']  === 'monthly') ? 'selected' : '' ; ?>
            												<option value="monthly" <?php echo $selected; ?>>monthly</option>            												
        											</select>	
											</td>
											<td>
												<div class="anfStatusCheck"></div>
												<div class="anfspinner"></div>
											</td>
											<td>
												<button onclick="anf_run_schudule_now(event, <?php echo $rr['id']; ?> );">Run Now</button>
											</td>
											<td>
												<button id="sss" onclick="anf_delete_schudule(event, <?php echo $rr['id']; ?>, this );">delete</button>
											</td>														
										 <?
										 		}
										 	}
										 ?></tr><?
									}	
								?>
							</tbody>
							</table>


												<!--Results table-->
					<table id="resultTable" class="wp-list-table widefat striped">
						<thead id="daveresult">
							<tr>
								<th>Title</th>
     							<th>Image</th>
     							<th>content</th>
							</tr>
						</thead>
					</table>

				<?								

					}	

				?>

				<div id="res"></div>

				<button  class="button button-primary" onclick="anf_save_schedule(event)">Save Changes</button>

			</form>

		<?php
		}
	}//end anf_advanced_tab_tab()




	/**
	* ==============================================================================	
	* ==============================================================================
	*	
	* set up query reference options tab url
	*
	* @since 1.0.0
	*
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================	
	**/
	public function anf_troubleshooting_init(){
		global $sd_active_tab; ?>
			<a class="nav-tab <?php echo $sd_active_tab == 'wcqv-troubleshooting' ? 'nav-tab-active' : ''; ?>" href="<?php echo admin_url( 'admin.php?page=action_news_feed&tab=wcqv-troubleshooting' ); ?>"><?php _e( 'Search Query Reference', 'action_news_feed' ); ?> </a>
		<?php
	}//End anf_troubleshooting_init()
 



	/**
	* ==============================================================================	
	* ==============================================================================
	*	
	* #### Query reference tab content #####
	*
	* @since 1.0.0
	*
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================	
	**/ 
	public function anf_troubleshooting_tab() {
		global $sd_active_tab;
			if ( 'wcqv-troubleshooting' != $sd_active_tab )
				return;
				
					if($this->anf_check_user_role( array('manage_options') ) == true ) {  ?>

						<h2><label for="apiTableSocial">Example Queries</label></h2>

							<?php
								
								$t = new Action_news_feed_query_builder_parts();

								echo $t->anf_build_query_ex_table();

					}

	}//end anf_troubleshooting_tab()






    /** 
	* ==============================================================================	
	* ==============================================================================
	*    
    * #### Sanitize each setting field as needed ####
    *
    * @since 1.0.0
    * @param array $input Contains all settings fields as array keys
    * @link https://codex.wordpress.org/Validating_Sanitizing_and_Escaping_User_Data
    **/
    private function anf_sanitize( $input ) {
        $new_input = array();
        		
        	//token
        	if( isset( $input['action_news_token'] ) )
            	$new_input['action_news_token'] = sanitize_text_field( $input['action_news_token'] );             

            //placeholderimage
            if( isset( $input['anf_feat_image_placeholder'] ) )
            	$new_input['anf_feat_image_placeholder'] = absint( $input['anf_feat_image_placeholder'] );   


            //show origin author cite
        	if( isset( $input['anf_show_origin_author_cite'] ) )
            	$new_input['anf_show_origin_author_cite'] = absint( $input['anf_show_origin_author_cite'] );   


            //show origin article url
        	if( isset( $input['anf_show_origin_url_cite'] ) )
            	$new_input['anf_show_origin_url_cite'] = absint( $input['anf_show_origin_url_cite'] );              



        	return $new_input;
    }//end sanitize




	/**
	* ==============================================================================	
	* ==============================================================================	
	*    	
	* render  action_news_token input
	*
	* @since 1.0.0
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================		
	**/
	public function action_news_token_render() {
		$options = get_option( 'action_news_feed' );
		$options['action_news_token'] = empty( $options['action_news_token'] ) ? '' : $options['action_news_token'];

			?>
				<input type='text' name='action_news_feed[action_news_token]'  value='<?php echo $options['action_news_token']; ?>'>

			<?php

	}// end action_news_token_render();


	/**
	* ==============================================================================	
	* ==============================================================================	
	*    	
	* ##### featuerd image placeholder for posts imported by this plugin ####
	*
	* @since 1.0.0
	* @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================		
	**/
	public function anf_feat_img_placeholder_render() {
		$options = get_option( 'action_news_feed' );
		$options['anf_feat_image_placeholder'] = empty( $options['anf_feat_image_placeholder'] ) ? '' : $options['anf_feat_image_placeholder'];

			?>
				<input type='text' name='action_news_feed[anf_feat_image_placeholder]'  value='<?php echo $options['anf_feat_image_placeholder']; ?>'>

			<?php

	}// end anf_feat_img_placeholder_render();




	/**
	* ==============================================================================	
	* ==============================================================================	
	*    	
	* #####  ####
	*
	* @since 1.0.0
	* @link     
	* ==============================================================================	
	* ==============================================================================		
	**/
	public function anf_show_origin_author_render() {


		if (!isset($anf_option_author)) $anf_option_author = get_option( 'action_news_feed' );
		$anf_option_author['anf_show_origin_author_cite'] = empty( $anf_option_author['anf_show_origin_author_cite'] ) ? '' : $anf_option_author['anf_show_origin_author_cite'];

		?>
			<input type='checkbox' name='action_news_feed[anf_show_origin_author_cite]' <?php checked( $anf_option_author['anf_show_origin_author_cite'], 1 ); ?> value='1'>

		<?php		

	}// end anf_show_origin_author_render();




	/**
	* ==============================================================================	
	* ==============================================================================	
	*    	
	* #####  ####
	*
	* @since 1.0.0
	* @link     
	* ==============================================================================	
	* ==============================================================================		
	**/
	public function anf_show_origin_url_render() {

		if (!isset($anf_option_url)) $anf_option_url = get_option( 'action_news_feed' );
		$anf_option_url['anf_show_origin_url_cite'] = empty( $anf_option_url['anf_show_origin_url_cite'] ) ? '' : $anf_option_url['anf_show_origin_url_cite'];

		?>
			<input type='checkbox' name='action_news_feed[anf_show_origin_url_cite]' <?php checked( $anf_option_url['anf_show_origin_url_cite'], 1 ); ?> value='1'>

		<?php

	}// end anf_show_origin_url_render();





    /**
	* ==============================================================================	
	* ==============================================================================	
	*    
    * ##### settings section callback function #####
    *
    * @since 	1.0.0
    * @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================	
    */
	public function action_news_feed_section_callback() { 

		echo __( '', 'action_news_feed' );

	}//end action_news_feed_section_callback()




    /**MAY not need this
	* ==============================================================================	
	* ==============================================================================	
	*    
    * settings section callback function
    *
    * @since 	1.0.0
    * @link     https://codex.wordpress.org/Settings_API
	* ==============================================================================	
	* ==============================================================================	
    */
	public function action_news_feed_troubleshooting_callback() { 

		?> tropubleshooting <?

	}//end action_news_feed_troubleshooting_callback



	/**
	* ==============================================================================	
	* ==============================================================================	
	*
	*
	*	
	* @since 	1.0.0
	* @link 	https://docs.woocommerce.com/document/adding-a-section-to-a-settings-tab/
	* @link 	https://www.ibenic.com/how-to-add-woocommerce-custom-product-fields/
	* ==============================================================================	
	* ==============================================================================		
	*/
	public function wcqv_custom_tab_action_render() {
  		?>
 			<li class="custom_tab">
    			<a href="#wcqv_custom_panel">
     				<span><?php _e( 'Product Quick View', 'product-quick-view-for-woocommerce' ); ?></span>
    			</a>
 			</li>
  		<?php
	}//end wcqv_custom_tab_action_render()




	/**
	* ==============================================================================	
	* ==============================================================================	
	*
	*
	*	
	* @since 		1.0.0
	* @link 		https://docs.woocommerce.com/document/adding-a-section-to-a-settings-tab/
	* @link 		https://www.ibenic.com/how-to-add-woocommerce-custom-product-fields/
	* @example 		echo get_post_meta( $post->ID, 'ID_GOES_HERE', true );
	* @example 		echo get_post_meta( get_the_ID(), 'ID_GOES_HERE', true );
	* ==============================================================================	
	* ==============================================================================		
	*/
	public function product_qv_wv_custom_tab() {
  		?>
  			<div id="wcqv_custom_panel" class="panel woocommerce_options_panel">
    			<div class="options_group">
      				<?php  

						$wcqv_chkbx_field = array(
							'id'            => '_checkbox', //slug
							'wrapper_class' => 'show_if_simple', 
							'label'         => __('Hide Quick View', 'product-quick-view-for-woocommerce' ), 
							'description'   => __( '<em>Hide Quick View on this product</em>', 'product-quick-view-for-woocommerce' ) 
        				);
        				// Checkbox
						woocommerce_wp_checkbox( $wcqv_chkbx_field );

     				?>
   				 </div>
  			</div>
		<?php
	}//end 	product_qv_wv_custom_tab()




	/**
	* ==============================================================================	
	* ==============================================================================		
	*
	*
	*
	* @since 	1.0.0
	* @link 	https://docs.woocommerce.com/document/adding-a-section-to-a-settings-tab/
	* @link 	https://www.ibenic.com/how-to-add-woocommerce-custom-product-fields/
	* @param	bool     $post_id     post ID
	* ==============================================================================	
	* ==============================================================================		
	*/
	public function product_qv_wv_custom_tab_save( $post_id ){

		// Checkbox
		$wcqv_woocommerce_checkbox = isset( $_POST['_checkbox'] ) ? 'yes' : 'no';
		update_post_meta( $post_id, '_checkbox', $wcqv_woocommerce_checkbox );

	}//end 	product_qv_wv_custom_tab_save()



	/*======================================================================================================================
	  ==========================================                           =================================================
	<-------------------------------------------| Import webhose functions |----------------------------------------------->
	============================================                           =================================================
	========================================================================================================================*/



	/**
	* ==============================================================================	
	* ==============================================================================
	*
	* Query the webhose API from user input, saves each post using wp_insert_post(),
	* then inserts featured image if one exists, returns JSON array back to ajax to
	* display results
	*
	* ==============================================================================
	* ==============================================================================	
	* @since  1.0.0 
	* @see   ./js/action-news-feed-admin.js
	* @see   './action-news-feed/admin/class-action-news-feed-admin.php - Generate_Featured_Image()'
	* @link  https://codex.wordpress.org/AJAX_in_Plugins
	* @link  https://github.com/Webhose/webhoseio-PHP
	* @link  https://developer.wordpress.org/reference/functions/wp_insert_post/
	*
	**/
	public function anf_quick_import_action() {

    	ob_start();

    	//nonce check
		if ( ! check_ajax_referer( 'anf-security-nonce', 'security' ) ) {

			$return = array('message' => "Invalid security token sent");

	         wp_send_json_error($return);

    		wp_die();
  		}    	

  		//verify user
  		if ( current_user_can( 'manage_options' ) ) {

			//passed in variables
			if (!isset($whatever)) $whatever = $_POST['whatever'];
			if (!isset($anfSearchQuery)) $anfSearchQuery =  $_POST['anfSearchQuery'];     // Search query from input box
			if (!isset($anfPostType)) $anfPostType =  $_POST['anfType'];               //post type
			if (!isset($anfCategory)) $anfCategory =  $_POST['anfcategory'];           // category
			if (!isset($anfNumber2import)) $anfNumber2import = $_POST['anfNumber2import'];  //number of posts to be inserted
			if (!isset($anfImportStatus)) $anfImportStatus = $_POST['anfImportStatus'];    //status publish/draft

	

		$this->anf_import_scheduled_cron_posts(array('query' => $anfSearchQuery, 'type' => $anfPostType, 'category' => $anfCategory, 'import_limit' => $anfNumber2import, 'post_status' => $anfImportStatus, ));


		
  					$response =ob_get_contents();
        			ob_end_clean();
        			echo $response;
        			die(1);

        }

	}//end anf_quick_import_action()




	/**
	* ==============================================================================	
	* ==============================================================================	
	*
	*  #### add additional intervals to wp-cron ####
	*
	* @since 1.0.0
	* @link https://developer.wordpress.org/plugins/cron/scheduling-wp-cron-events/
	* @link https://codex.wordpress.org/Plugin_API/Filter_Reference/cron_schedules
	* ==============================================================================	
	* ==============================================================================
	**/
	public function anf_add_cron_interval() {
		//5 mins for testing remove in production
		$schedules['5min'] = array(
			'interval' => 5*60,
			'display' => __('Once every 5 minutes for testing')
		);		
		// add a 'weekly' interval
		$schedules['weekly'] = array(
			'interval' => 604800,
			'display' => __('Once Weekly')
		);
		//add a 'monthly interval'
		$schedules['monthly'] = array(
			'interval' => 2635200,
			'display' => __('Once a month')
		);
		
		return $schedules;

	}//end anf_add_cron_interval()




	/**
	* ==============================================================================	
	* ==============================================================================	
	* 
	*
	*
	* @since 1.0.0
	* @link
	* @link
	* ==============================================================================	
	* ==============================================================================
	**/
	public function anf_schedule_crons() {
		$r = get_option('anf_schedule_rows_opts');

		foreach ($r as $rr) {
			foreach ($rr as  $t2) {
				if(is_array($t2)) {
					//if($t2['status'] == 1  ) {
						$args = array( $rr['id'] );
							switch ($t2) {
								case $t2['status'] == 1 && $t2['schedule_cron'] == 'daily':
									if ( ! wp_next_scheduled( 'anf_daily_cron_exec', $args ) ) {
    									wp_schedule_event( time(), 'daily', 'anf_daily_cron_exec', $args );
									}
								break;
								case $t2['status'] == 1 && $t2['schedule_cron'] == 'weekly':
									if ( ! wp_next_scheduled( 'anf_weekly_cron_exec', $args ) ) {
    									wp_schedule_event( time(), 'weekly', 'anf_weekly_cron_exec', $args );
									}
								break;
								case $t2['status'] == 1 &&  $t2['schedule_cron'] == 'monthly':
									if ( ! wp_next_scheduled( 'anf_montlhy_cron_exec', $args ) ) {
    									wp_schedule_event( time(), 'montlhy', 'anf_montlhy_cron_exec', $args );
									}
								break;
								default:
									wp_clear_scheduled_hook( 'anf_'.$t2['schedule_cron'].'_cron_exec', $args );
								}
								 			

				}
			}
		}	 	
	}// end anf_schedule_crons();




	/**
	* ==============================================================================	
	* ==============================================================================	
	*
	* execute the daily cron schedule import
	*
	* @since 1.0.0
	* @param array   $params    array passed from wp_schedule_event
	* @see ../admin/class-action-news-feed.php -> anf_schedule_crons()
	* ==============================================================================	
	* ==============================================================================	
	**/
	public function anf_daily_cron_exec( $params ) {

		$r = get_option('anf_schedule_rows_opts');
			if($params) {
				foreach ($r as $rr) {
					foreach ($rr as  $t2) {
						if(is_array($t2)) {
							if($t2['status'] == 1 && $t2['schedule_cron'] == 'daily' ) {
							$msg = "".$t2['query']."";

								//run the import function
								$this->anf_import_scheduled_cron_posts(array('query' => $t2['query'], 'type' => $t2['type'], 'category' => $t2['category'], 'import_limit' => $t2['import_limit'], 'post_status' => $t2['post_status'], ));
							}
						}
					}
				}		
			}
	
	}//end anf_daily_cron_exec();




	/**
	* ==============================================================================	
	* ==============================================================================	
	*
	* 	execute the weekly cron schedule import
	*
	* @since 1.0.0
	* @param array   $params    array passed from wp_schedule_event
	* @see ../admin/class-action-news-feed.php -> anf_schedule_crons()
	* ==============================================================================	
	* ==============================================================================	
	**/
	public function anf_weekly_cron_exec( $params ) {

		$r = get_option('anf_schedule_rows_opts');
			if($params) {
				foreach ($r as $rr) {
					foreach ($rr as  $t2) {
						if(is_array($t2)) {
							if($t2['status'] == 1 && $t2['schedule_cron'] == 'weekly' /*&& $rr['id'] == $params[0] */) {
								//$msg = "".$t2['query']."";

								//run the import function
								$this->anf_import_scheduled_cron_posts(array('query' => $t2['query'], 'type' => $t2['type'], 'category' => $t2['category'], 'import_limit' => $t2['import_limit'], 'post_status' => $t2['post_status'], ));
							}
						}
					}
				}		
			}
	}//end anf_weekly_cron_exec()




	/**
	* ==============================================================================	
	* ==============================================================================	
	*	
	* execute the monthly cron schedule import
	*
	* @since 1.0.0
	* @param array   $params    array passed from wp_schedule_event
	* @see ../admin/class-action-news-feed.php -> anf_schedule_crons()
	* ==============================================================================	
	* ==============================================================================		
	**/
	public function anf_monthly_cron_exec( $params ) {

		$r = get_option('anf_schedule_rows_opts');
			if($params) {
				foreach ($r as $rr) {
					foreach ($rr as  $t2) {
						if(is_array($t2)) {
							if($t2['status'] == 1 && $t2['schedule_cron'] == 'monthly' /*&& $rr['id'] == $params[0] */) {
								//$msg = "".$t2['query']."";

								//run the import function
								$this->anf_import_scheduled_cron_posts(array('query' => $t2['query'], 'type' => $t2['type'], 'category' => $t2['category'], 'import_limit' => $t2['import_limit'], 'post_status' => $t2['post_status'], ));
							}
						}
					}
				}			
			}
	}//end anf_monthly_cron_exec()





    /**
	* ==============================================================================	
	* ==============================================================================	
	*
	*
	*
    * @since 1.0.0
	* ==============================================================================	
	* ==============================================================================	
    **/
    public function anf_run_schedule_ondemand() {

    	ob_start();

		if ( ! check_ajax_referer( 'anf-security-nonce', 'security' ) ) {

			$return = array('message' => "Invalid security token sent");

	         wp_send_json_error($return);

    		wp_die();
  		}      	


  		 //verify user
  		if ( current_user_can( 'manage_options' ) ) {

		$query = $_POST['query'];
		$type = $_POST['type'];
		$category = $_POST['category'];
		$import_limit = $_POST['import_limit'];
		$post_status = $_POST['post_status'];

			$this->anf_import_scheduled_cron_posts(array('query' => $query, 'type' => $type, 'category' => $category, 'import_limit' => $import_limit, 'post_status' => $post_status ));

		}

		
    	$response =ob_get_contents();
        ob_end_clean();
        echo $response;
        die(1);
    }//end anf_run_schedule_ondemand();





	/**
	* ==============================================================================	
	* ==============================================================================
	*	
	* import posts function from scheduled crons
	*	
	* @since  1.0.0
	* @param  array    $args    array of arguments that for the importer, i.e search wquery, post category
	* @var    string   $anfSearchQuery    passed in search query
	* @var    string   $anfPostType       passed in post type
	* @var    string   $anfCategory       passed in category
	* @var    string   $anfNumber2import  passed in import limit
	* @var    string   $anfImportStatus   passed in post status
	* @example $args = array('query' => 'sports news', 'type' => 'page', 'category' => 'Sports', 'import_limit' => 20, 'post_status' => 'draft',);  $this->anf_import_scheduled_cron_posts( $args );
	* ==============================================================================	
	* ==============================================================================	
	*/
	private function anf_import_scheduled_cron_posts( $args ) {

		$anfSearchQuery =  $args['query'];     		// Search query from input box
		$anfPostType =  $args['type'];              //post type
		$anfCategory =  $args['category'];          // category
		$anfNumber2import = $args['import_limit'];  //number of posts to be inserted
		$anfImportStatus = $args['post_status'];    //status publish/draft



         
			//get the saved token
			$options = get_option( 'action_news_feed' );
			$anf_token = $options['action_news_token'];


			//if post insert tracker option doesnot exist creat it
			$option_name = 'anf_created_posts';
			$new_value = array( );

				if ( get_option( $option_name ) == false ) {
 
					$deprecated = null;
    				$autoload = 'no';
    				add_option( $option_name, $new_value, $deprecated, $autoload );
				
				}


			//if webhose request racker option not exist then create it
			$webhoseReqTrackerName = 'anf_webhose_req_count';
			$webhoseNewVal = 0;
				if ( get_option( $webhoseReqTrackerName ) == false ) {
					$deprecated2 = null;
    				$autoload2 = 'no';
    				add_option( $webhoseReqTrackerName, $webhoseNewVal, $deprecated2, $autoload2 );
				}


			//use ./webhose.php to query the api request
    		Webhose::config("".$anf_token."");

				$params2 = array("q"=>"".$anfSearchQuery." language:english", "size"=>$anfNumber2import,  "sort" => "crawled",);
				//$params2 = array("q"=>"".$anfSearchQuery." language:english", "size"=>2,  "sort" => "crawled",);
    			//$params3 = array("q"=>"performance_score:>0 language:english", "size"=>1,  "sort" => "crawled",);
    		
    			$result = Webhose::query("filterWebContent", $params2);

				//array to pass to JS file - contains api title | image | content
				$outerresarray = array();

        				if($result == null)
        				{
            				echo "<p>Response is null, no action taken.</p>";
            				return;
        				}
        				if(isset($result->posts))
            				foreach($result->posts as $post)
            				{
            					if($post->title !== null) {


            				
								$title = sanitize_text_field($post->title);
								$featImage = $post->thread->main_image;
								$slug = str_replace(' ', '-',  $post->title);
								$content = sanitize_textarea_field($post->text);
								$expert = substr($content, 0, 250);
								$OriginAuthor = sanitize_text_field($post->author);
								$orignUrl = $post->url;								
            					global $user_ID;
									
									$new_post = array(
										'post_title' => ''.$post->title.'',
										'post_content' => ''.$content.'',
										'post_status' => $anfImportStatus,
										'post_date' => date('Y-m-d H:i:s'),
										'post_author' => $user_ID,
										'post_type' => $anfPostType,
										'post_category' => array( $anfCategory )
									);
									$post_id = wp_insert_post($new_post);

									//origin author meta box
									if (!isset($authorMetaBox)) $authorMetaBox = get_post_meta($post->ID, '_anf_meta_author_key', true); 

									//update the meta author box
									update_post_meta( $post_id, '_anf_meta_author_key', $OriginAuthor );

									//update the meta origin URL
									update_post_meta( $post_id, '_anf_meta_originUrl_key', $orignUrl );
									



									//featured image
									if( @getimagesize($featImage)) {//if not empty and url is good


										$mimes = get_allowed_mime_types();


										$url = strtok($featImage, '?');//remove query string
										$file_parts = pathinfo($url);//get url parts as array

											if($file_parts['extension'] == "" || $file_parts['extension'] == null) {//check if file extension

												if(array_key_exists($file_parts['extension'], $mimes)) {//if allowed in wp media libarary
													
													//insert placeholder image
													$this->Generate_Featured_Image( 'https://satrnnews.com/wp-content/uploads/2019/01/satrnplchlderblk.png',   $post_id  );													
												}


											} else {//not savable to wp

												//insert the featured image
												$this->Generate_Featured_Image( strtok($featImage, '?'),   $post_id  );												
											}


									} else {

										//insert placeholder
										$this->Generate_Featured_Image( 'https://satrnnews.com/wp-content/uploads/2019/01/satrnplchlderblk.png',   $post_id  );
									}
									
									//post tracker
									$anfTracker = get_option('anf_created_posts');

									array_push( $anfTracker, $post_id );

    								update_option( $option_name, $anfTracker );



									//add elements to array to send back to js
									$resarray = array();
                						$resarray['post_title']  = $post->title;
										$resarray['post_text']  = $post->text;
										$resarray['post_image'] = $post->thread->main_image;
  
										//push this single post array to the main array
										array_push($outerresarray,$resarray);





									//insert category as tag to help with related posts
									if (!isset($anf_cat_name)) $anf_cat_name = get_cat_name( $anfCategory );
									wp_set_post_tags( $post_id, array( $anf_cat_name ), true );



								}


            				}

            			//update req tracker option
            			update_option( 'anf_webhose_req_count', $result->requestsLeft );	

					print json_encode($outerresarray);//send array

         
            		
	}//end anf_import_scheduled_cron_posts()


	/**
	* ==============================================================================	
	* ==============================================================================
	*	
	* On delete post -> if post was creatd by this plugin then delete
	*
	* @since 1.0.0
	*
	* ==============================================================================	
	* ==============================================================================	
	**/
	public function wps_remove_attachment_with_post($post_id_alt) {

 		if(in_array( $post_id_alt, $this->AnfCreatedPosts)) {

    		if(has_post_thumbnail( $post_id_alt )) {

      			$attachment_id = get_post_thumbnail_id( $post_id_alt );
      			wp_delete_attachment($attachment_id, true);
    		}
		}
 
	}


	/**
	* ==============================================================================	
	* ==============================================================================
	*
	* ####  ####
	*
	* @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	* ==============================================================================	
	* ==============================================================================	
	*/
	public function anf_add_custom_box_author() {

		if (!isset($anf_screens)) $anf_screens = ['post', 'wporg_cpt'];
    		foreach ($anf_screens as $screen) {
        		add_meta_box(
            		'wporg_box_id',           // Unique ID
            		'Action News Feed Origin Article Author',  // Box title
            		array( $this, 'anf_custom_box_author_html'),  
            		$screen,                   // Post type
            		'normal',
            		'high'
        		);
    		}

	}//end anf_add_custom_box_author()




	/**
	* ==============================================================================	
	* ==============================================================================
	*
	* #### This renders the html for the origin author meta box ####
	*
	* @param obj     $post     post object
	* @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	* ==============================================================================	
	* ==============================================================================	
	*/
	public function anf_custom_box_author_html($post) {

		//retrieve the value
		if (!isset($anf_origin_author_val)) $anf_origin_author_val = get_post_meta($post->ID, '_anf_meta_author_key', true); ?>
    	
    		<label for="anf_author_field">When you import articles from other resources you may need to site the author:</label><br>

				<?php  $anf_origin_author_val = empty(  $anf_origin_author_val ) ? '' :  $anf_origin_author_val; ?>

					<input id="anf_author_field" class="" type='text' placeholder="Origin Author" name='anf_author_field'  value='<?php echo $anf_origin_author_val; ?>'/>
    	<?php
	}//end anf_custom_box_author_html



	/**
	* ==============================================================================	
	* ==============================================================================
	*
	* #### This saves the origin author meta box field ####
	* 
	* @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	* ==============================================================================	
	* ==============================================================================	
	*/
	public function anf_save_postdata_author($post_id) {
    
    	if (array_key_exists('anf_author_field', $_POST)) {
        	update_post_meta(
            	$post_id,
            	'_anf_meta_author_key',
            	$_POST['anf_author_field']
        	);
    	}
	}//end anf_save_postdata_author() 






	/**
	* ==============================================================================	
	* ==============================================================================
	*
	* #### Add custom meta box for the origin article url in case you want to cite the url ####
	*
	* @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	* ==============================================================================	
	* ==============================================================================	
	*/
	public function anf_add_custom_box_origin_url() {

		if (!isset($anf_screens)) $anf_screens = ['post',];
    		foreach ($anf_screens as $screen) {
        		add_meta_box(
            		'wporg_box_id_origin_url',           // Unique ID
            		'Action News Feed Origin Article URL',  // Box title
            		array( $this, 'anf_custom_box_author_origin_url'),  
            		$screen,                   // Post type
            		'normal',
            		'high'
        		);
    		}

	}//end anf_add_custom_box_origin_url()





	/**
	* ==============================================================================	
	* ==============================================================================
	*
	* #### This saves the origin url meta box field ####
	*
	* @see $this->anf_add_custom_box_origin_url()
	* @param obj     $post     post object
	* @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	* ==============================================================================	
	* ==============================================================================	
	*/
	public function anf_custom_box_author_origin_url($post) {

		//retrieve the value
		if (!isset($anf_meta_url_val)) $anf_meta_url_val = get_post_meta($post->ID, '_anf_meta_originUrl_key', true); ?>
    	
    		<label for="anf_origin_url_field">When you import articles from other resources you may need to site the author:</label><br>

				<?php  $anf_meta_url_val = empty(  $anf_meta_url_val ) ? '' :  $anf_meta_url_val; ?>

					<input id="anf_origin_url_field" class="" type='text' placeholder="Origin Article URL" name='anf_origin_url_field'  value='<?php echo $anf_meta_url_val; ?>'/>
    	<?php
	}//end anf_custom_box_author_origin_url





	/**
	* ==============================================================================	
	* ==============================================================================
	*
	* #### save article origin url ####
	*
	* @link https://developer.wordpress.org/plugins/metadata/custom-meta-boxes/
	* ==============================================================================	
	* ==============================================================================	
	*/
	public function anf_save_postdata_origin_url($post_id) {
    
    	if (array_key_exists('anf_origin_url_field', $_POST)) {
        	update_post_meta(
            	$post_id,
            	'_anf_meta_originUrl_key',
            	$_POST['anf_origin_url_field']
        	);
    	}

	}//end anf_save_postdata_origin_url() 





	/**
	* ==============================================================================	
	* ==============================================================================
	*
	* #### This adds the cite after the content on single posts if the option is checked ####
	*
	* @since 1.0.0
	* @link 
	* ==============================================================================	
	* ==============================================================================	
	*/
	public function anf_add_cite_to_content( $content ) {  

		//get origin author cite option value
		if (!isset($options)) $options = get_option( 'action_news_feed' );


		$options['anf_show_origin_author_cite'] = empty( $options['anf_show_origin_author_cite'] ) ? '' : $options['anf_show_origin_author_cite'];
		$options['anf_show_origin_url_cite'] = empty( $options['anf_show_origin_url_cite'] ) ? '' : $options['anf_show_origin_url_cite'];


    		if( is_single() && ! empty( $GLOBALS['post'] ) ) {

    			if ( $GLOBALS['post']->ID == get_the_ID() ) {

    				//get the id
    				if (!isset($anf_ID)) $anf_ID = get_the_ID();

    					//get the id of the post
						if($options['anf_show_origin_url_cite'] == 1 ) {


							  if(get_post_meta(get_the_ID(), "_anf_meta_originUrl_key", true)) { 


    								$anf_url_org = get_post_meta(get_the_ID(), '_anf_meta_originUrl_key', true);
    								$content .= '<p class="anf_origin_url_cite"><a target="_blank" href="'.html_entity_decode($anf_url_org).'">Cited by url: '.html_entity_decode($anf_url_org).'</a></p>';		
								}							

						}

						if($options['anf_show_origin_author_cite'] == 1 ) {

							  if(get_post_meta(get_the_ID(), "_anf_meta_author_key", true)) { 

    								$anf_author_org = get_post_meta(get_the_ID(), '_anf_meta_author_key', true);
    								$content .= '<p class="anf_origin_author_cite">Cited by author: '.html_entity_decode($anf_author_org).'</p>';
								}									
						}

        		}

    		}
    	
    	return $content;
	}



/*
notes: author url isnot saving check it

*/






}


}

/**
*
* @since 1.0.0
* @see  '/action-news-feed/admin/class-action-news-feed-admin.php -> my_action()'
**/

//if (!isset($webhose_config_path)) $webhose_config_path = '/admin/webhose.php';
//	if(validate_file($webhose_config_path)) {
		require_once(dirname(plugin_dir_path(__FILE__)) . '/admin/webhose.php');
//	}
require_once(dirname(plugin_dir_path(__FILE__)) . '/admin/query_builder_parts.php');
