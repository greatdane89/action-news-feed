<?php

/**
 * Fired during plugin activation
 *
 * @link       
 * @since      1.0.0
 *
 * @package    Action News Feed
 * @subpackage Action News Feed/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Action News Feed
 * @subpackage Action News Feed/includes
 * @author     Your Name <email@example.com>
 */
class Action_news_feed_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {


		  		set_transient( '_action_news_feed_welcome_screen_activation_redirect', true, 30 );


				$option_name = 'anf_schedule_rows_opts';
					$r = array(
					    array(
					        'id' => '123', 
					        'body' =>  array(
                                	'status' => 0,
								 	'query' => 'performance_score:>5 spam_score:<.8 site_category:technology language:english site_type:news',
								 	'type' => 'post',
								 	'category' => 'Uncategorised',
								 	'schedule_cron' => '',
								 	'post_status' => 'draft',
								 	'import_limit' => '3'
							)
					    ),				    
					);					


    				$deprecated = null;
    				$autoload = 'no';
    				add_option( $option_name, $r, $deprecated, $autoload );

    				//featured image placeholder
					$option_name2 = 'anf_feat_image_placeholder';
    				$deprecated2 = null;
    				$autoload2 = 'no';
    				add_option( $option_name2, $r, $deprecated2, $autoload2 );

    				//featured image placeholder
					$option_name3 = 'anf_webhose_req_count';
    				$deprecated3 = null;
    				$autoload3 = 'no';
    				add_option( $option_name3, $r, $deprecated3, $autoload3 );    				


	}

}
