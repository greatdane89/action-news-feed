<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     David Baty
 */
class Action_news_feed {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Plugin_Name_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $action_news_feed    The string used to uniquely identify this plugin.
	 */
	protected $action_news_feed;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'ACTION_NEWS_FEED_VERSION' ) ) {
			$this->version = ACTION_NEWS_FEED_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->action_news_feed = 'action-news-feed';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Action_news_feed_Loader. Orchestrates the hooks of the plugin.
	 * - Action_news_feed_i18n. Defines internationalization functionality.
	 * - Action_news_feed_Admin. Defines all hooks for the admin area.
	 * - Action_news_feed_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-action-news-feed-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-action-news-feed-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-action-news-feed-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-action-news-feed-public.php';

		$this->loader = new Action_news_feed_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Plugin_Name_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Action_news_feed_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Action_news_feed_Admin( $this->get_action_news_feed(), $this->get_version() );


		//create welcome page
		$this->loader->add_action('admin_menu', $plugin_admin, 'welcome_screen_pages');
		
		//redirect welcom page
		$this->loader->add_action( 'admin_init', $plugin_admin, 'welcome_screen_do_activation_redirect' );

		//remove welcom page
		$this->loader->add_action( 'admin_head', $plugin_admin, 'welcome_screen_remove_menus' );


		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );




		//delete feat image when posts are deleted if tehy where created by this plugin
		$this->loader->add_action('before_delete_post', $plugin_admin, 'wps_remove_attachment_with_post', 1 );



		//add settings page
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'action_news_feed_add_admin_menu' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'action_news_feed_settings_init' );

			//settings page tabs
			$this->loader->add_action( 'anf_settings_tab',  $plugin_admin,'anf_general_tab_init', 1 );
			$this->loader->add_action( 'anf_settings_content', $plugin_admin,'anf_general_render_options_tab' );
			$this->loader->add_action( 'anf_settings_tab', $plugin_admin,'anf_advanced_tab_init' );
			$this->loader->add_action( 'anf_settings_content',$plugin_admin, 'anf_advanced_tab_tab' );
			$this->loader->add_action( 'anf_settings_tab', $plugin_admin,'anf_troubleshooting_init' );
			$this->loader->add_action( 'anf_settings_content',$plugin_admin, 'anf_troubleshooting_tab' );





	//ajax quick import	
	$this->loader->add_action( 'wp_ajax_anf_quick_import_action', $plugin_admin, 'anf_quick_import_action' );
	$this->loader->add_action( 'wp_ajax_nopriv_anf_quick_import_action', $plugin_admin, 'anf_quick_import_action' );

	
	//ajax run schedule import	
	$this->loader->add_action( 'wp_ajax_anf_run_schedule_now', $plugin_admin, 'anf_run_schedule_now' );
	$this->loader->add_action( 'wp_ajax_nopriv_anf_run_schedule_now', $plugin_admin, 'anf_run_schedule_now' );

	//ajax run schedule delete	
	$this->loader->add_action( 'wp_ajax_anf_delete_schudule', $plugin_admin, 'anf_delete_schudule' );
	$this->loader->add_action( 'wp_ajax_nopriv_anf_delete_schudule', $plugin_admin, 'anf_delete_schudule' );

	//ajax run schedule save	
	$this->loader->add_action( 'wp_ajax_anf_save_schedule', $plugin_admin, 'anf_save_schedule' );
	$this->loader->add_action( 'wp_ajax_nopriv_anf_save_schedule', $plugin_admin, 'anf_save_schedule' );

	//ajax run schedule on demenad	
	$this->loader->add_action( 'wp_ajax_anf_run_schedule_ondemand', $plugin_admin, 'anf_run_schedule_ondemand' );
	$this->loader->add_action( 'wp_ajax_nopriv_anf_run_schedule_ondemand', $plugin_admin, 'anf_run_schedule_ondemand' );


	//delete created posts
	$this->loader->add_action( 'wp_ajax_anf_delete_created_posts', $plugin_admin, 'anf_delete_created_posts' );
	$this->loader->add_action( 'wp_ajax_nopriv_anf_delete_created_posts', $plugin_admin, 'anf_delete_created_posts' );

	//reset cron schedules
	$this->loader->add_action( 'wp_ajax_anf_reset_schedule', $plugin_admin, 'anf_reset_schedule' );
	$this->loader->add_action( 'wp_ajax_nopriv_anf_reset_schedule', $plugin_admin, 'anf_reset_schedule' );

	//add addition filters to wp-cron
	$this->loader->add_filter('cron_schedules', $plugin_admin, 'anf_add_cron_interval');


	$this->loader->add_action( 'anf_daily_cron_exec', $plugin_admin, 'anf_daily_cron_exec' );
	$this->loader->add_action( 'anf_weekly_cron_exec', $plugin_admin, 'anf_weekly_cron_exec' );
	$this->loader->add_action( 'anf_monthly_cron_exec', $plugin_admin, 'anf_monthly_cron_exec' );



	//add post meta for author
	$this->loader->add_action('add_meta_boxes', $plugin_admin, 'anf_add_custom_box_author');
	//save meta field for author
	$this->loader->add_action('save_post', $plugin_admin, 'anf_save_postdata_author');



	//add post meta for author
	$this->loader->add_action('add_meta_boxes', $plugin_admin, 'anf_add_custom_box_origin_url');
	//save meta field for author
	$this->loader->add_action('save_post', $plugin_admin, 'anf_save_postdata_origin_url');

	
	//adds the cite to the content of post
	$this->loader->add_filter( 'the_content', $plugin_admin, 'anf_add_cite_to_content' );



	//load scripst for media uploader options
	$this->loader->add_action( 'admin_footer', $plugin_admin, 'anf_print_media_uploader_scripts' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Action_news_feed_Public( $this->get_action_news_feed(), $this->get_version() );

		//$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		//$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_action_news_feed() {
		return $this->action_news_feed;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Plugin_Name_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
