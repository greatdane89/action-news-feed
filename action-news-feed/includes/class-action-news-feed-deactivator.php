<?php

/**
 * Fired during plugin deactivation
 *
 * @link       
 * @since      1.0.0
 *
 * @package    Action News Feed
 * @subpackage Action News Feed/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Action News Feed
 * @subpackage Action News Feed/includes
 * @author     David Baty
 */
class Action_news_feed_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {


	}

}
