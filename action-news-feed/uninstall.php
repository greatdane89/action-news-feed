<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however, this is the
 * general skeleton and outline for how the file should work.
 *
 * For more information, see the following discussion:
 * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/pull/123#issuecomment-28541913
 *
 * @link       
 * @since      1.0.0
 *
 * @package    Action News Feed
 */

// If uninstall not called from WordPress, then exit.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}


if (!isset($wcqv_optionanf{ 
  $anf_options = get_option( 'action_news_feed' );
}

function wcqv_plugin_deactivation() {


  foreach ($wcqv_options as $wcqvToDelete ) {
    delete_option( $wcqvToDelete );
  }

		//remove crons
		wp_clear_scheduled_hook( 'anf_daily_cron_exec' );
		wp_clear_scheduled_hook( 'anf_weekly_cron_exec' );
		wp_clear_scheduled_hook( 'anf_monthly_cron_exec' );

		//remove options
		delete_option('action_news_feed');
		delete_option('anf_schedule_rows_opts');
		delete_option('anf_created_posts');
		delete_option('anf_feat_image_placeholder');
		delete_option('anf_webhose_req_count');

}	